import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
import torch
from torch.autograd import Variable
from matplotlib import pyplot as plt
import pickle as pkl
from torch_geometric.data import Data, DataLoader
from tqdm import tqdm
import math
import random

# from potential_GNN import GN_original, GN_potential
from potential_GNN_model_ours import GN_potential_ours_SiLU #, GN_potential_ours_ReLU, GN_potential_ours_GELU, GN_potential_ours_Tanh, GN_potential_ours_Softplus, GN_potential_ours_Sigmoid, GN_potential_ours_LeakyReLU
from potential_GNN_model_original import GN_potential_original_ReLU #, GN_potential_original_SiLU

np.random.rand(123)
torch.manual_seed(123)
random.seed(123)


# model_flag = "ours"
# model_flag = "original"


## the simulation type
# sim_type = "spring"
# sim_type = "charge"
# sim_type = "orbital"
# sim_type = "discontinuous"


# simulation dimension
# data_dim = 3 # 
# data_dim = 2

for model_flag in ["ours"]: # "original" for baseline; "ours" for PIGNPI
    # for sim_type in ["spring", "charge", "orbital", "discontinuous"]:
    for sim_type in ["spring"]:
        # for data_dim in [2, 3]:
        for data_dim in [2]:
            # for act_func in ["SiLU", "ReLU", "GELU", "tanh", "sigmoid", "softplus", "LeakyReLU"]:
            for act_func in ["SiLU"]:
                if model_flag == "ours":
                    saving_dir = "./trained_model_{}/ours_{}_{}/".format(sim_type, act_func, data_dim)
                    print("--------\nEvaluate OUR model with {}".format(act_func) )
                elif model_flag == "original":
                    saving_dir = "./trained_model_{}/original_{}_{}/".format(sim_type, act_func, data_dim)
                    print("--------\nEvaluate ORIGINAL model with {}".format(act_func) )
                else:
                    print("saving_dir error")
                    exit()
                assert os.path.isdir(saving_dir)



                torch.set_default_dtype(torch.float64)
                square_flag = False
                augment_flag = False



                # assert we have gpu
                print(torch.cuda.device_count())
                print(torch.ones(1).cuda())

                
                simulation_data_dir = "../simulation/{}_simulation/generalization/".format(sim_type)
                with open(simulation_data_dir+ "12particles_{}_dim{}.pkl".format(sim_type, data_dim), "rb") as f:
                    simulation_data = pkl.load(f)
                    print("loading " + simulation_data_dir+ "12particles_{}_dim{}.pkl".format(sim_type, data_dim) )
                acc = simulation_data["node_acc"]
                num_experiments, simulation_steps, num_nodes, dim = acc.shape
                print("num_experiments, simulation_steps, num_nodes, dim")
                print(num_experiments, simulation_steps, num_nodes, dim)



                ## load the force and potential on edges
                with open(simulation_data_dir+ "12particles_force_potential_{}_dim{}.pkl".format(sim_type, data_dim), "rb") as f:
                    force_potential_groundTruth = pkl.load(f)
                    print("loaded " + simulation_data_dir+ "12particles_force_potential_{}_dim{}.pkl".format(sim_type, data_dim) )
                    edge_force = force_potential_groundTruth['edge_force']
                    edge_potential = force_potential_groundTruth['edge_potential']

                print("edge_force shape")
                print(edge_force.shape)
                print("edge_potential shape")
                print(edge_potential.shape)

                 

                # edge_list and edge_feature
                edge_list = []
                # edge_feature = []
                for this_edge in simulation_data["edge_list"]:
                    edge_list.append([this_edge[0], this_edge[1] ] ) # edge from i to j
                    # edge_feature.append([this_edge[2] ] )
                    edge_list.append([this_edge[1], this_edge[0] ] ) # edge from j to i
                    # edge_feature.append([this_edge[2] ] )
                edge_list = np.array(edge_list) # shape [num_edge, 2]
                # edge_feature = np.array(edge_feature) # shape [num_edge, 1], 1 is the spring constant
                
                

                ## construct feature
                # mass
                node_mass = np.array(simulation_data["node_mass"])
                print("node_mass: {}".format(node_mass ) )
                node_mass = node_mass[:, np.newaxis]
                node_mass = np.tile(node_mass, (num_experiments, simulation_steps, 1, 1) )
                print("node_mass shape: {}".format( node_mass.shape) )


                # charge
                node_charge = np.array(simulation_data["node_charge"])
                print("node_charge: {}".format(node_charge) )
                node_charge = node_charge[:, np.newaxis]
                node_charge = np.tile(node_charge, (num_experiments, simulation_steps, 1, 1) )
                print("node_charge shape: {}".format( node_charge.shape) )

                # node feature
                node_feature = np.concatenate((simulation_data["node_location"], simulation_data["node_velocity"], node_charge, node_mass), axis=-1)
                # node_feature = np.concatenate((simulation_data["node_location"], node_mass), axis=-1)
                print(node_feature.shape)

                node_potential = simulation_data["node_potential"]
                print("node_potential shape")
                print(node_potential.shape)


                ## split training dataset and testing dataste
                training_node_X = []
                training_node_Y = []
                training_edge_force = [] #for evaluting the pairwise force on edges
                training_edge_potential = [] #for evaluting the potential on edges
                training_node_potential = []

                valid_node_X = []
                valid_node_Y = []
                valid_edge_force = []
                valid_edge_potential = []
                valid_node_potential = []

                testing_node_X = []
                testing_node_Y = []
                testing_edge_force = []
                testing_edge_potential = []
                testing_node_potential = []

                # for flattening first two dimensions
                node_feature_flatten = []
                acc_flatten = []
                edge_force_flatten = []
                edge_potential_flatten = []
                node_potential_flatten = []

                for i in range(num_experiments):
                    for j in range(simulation_steps):
                        node_feature_flatten.append(node_feature[i, j] )
                        acc_flatten.append(acc[i,j])
                        edge_force_flatten.append(edge_force[i,j])
                        edge_potential_flatten.append(edge_potential[i, j] )
                        node_potential_flatten.append(node_potential[i, j])


                training_idx_list = simulation_data["training_idx"]
                val_idx_list = simulation_data["val_idx"]
                test_idx_list = simulation_data["test_idx"]



                # training dataset
                training_idx_list = training_idx_list[0 : len(training_idx_list) // 5]
                for train_idx in training_idx_list:
                    training_node_X.append(node_feature_flatten[train_idx])
                    training_node_Y.append(acc_flatten[train_idx])
                    training_edge_force.append(edge_force_flatten[train_idx] )
                    training_edge_potential.append(edge_potential_flatten[train_idx])
                    training_node_potential.append(node_potential_flatten[train_idx])

                # valid dataset
                val_idx_list = val_idx_list[0 : len(val_idx_list) // 5]
                for val_idx in val_idx_list:
                    valid_node_X.append(node_feature_flatten[val_idx])
                    valid_node_Y.append(acc_flatten[val_idx])
                    valid_edge_force.append(edge_force_flatten[val_idx])      
                    valid_edge_potential.append(edge_potential_flatten[val_idx])
                    valid_node_potential.append(node_potential_flatten[val_idx])

                # testing dataset
                test_idx_list = test_idx_list[0 : len(test_idx_list) // 5]
                for test_idx in test_idx_list:
                    testing_node_X.append(node_feature_flatten[test_idx])
                    testing_node_Y.append(acc_flatten[test_idx])
                    testing_edge_force.append(edge_force_flatten[test_idx])
                    testing_edge_potential.append(edge_potential_flatten[test_idx])
                    testing_node_potential.append(node_potential_flatten[test_idx])
                        


                training_node_X = np.array(training_node_X)
                training_node_Y = np.array(training_node_Y)
                training_edge_force = np.array(training_edge_force)
                training_edge_potential = np.array(training_edge_potential)
                training_node_potential = np.array(training_node_potential)

                valid_node_X = np.array(valid_node_X)
                valid_node_Y = np.array(valid_node_Y)
                valid_edge_force = np.array(valid_edge_force)
                valid_edge_potential = np.array(valid_edge_potential)
                valid_node_potential = np.array(valid_node_potential)

                testing_node_X = np.array(testing_node_X)
                testing_node_Y = np.array(testing_node_Y)
                testing_edge_force = np.array(testing_edge_force)
                testing_edge_potential = np.array(testing_edge_potential)
                testing_node_potential = np.array(testing_node_potential)


                print("training_node_X shape: {}".format(training_node_X.shape) )
                print("training_node_Y shape: {}".format(training_node_Y.shape) )
                print("training_edge_force shape: {}".format(training_edge_force.shape) )
                print("training_edge_potential shape: {}".format(training_edge_potential.shape) )

                print("valid_node_X shape: {}".format(valid_node_X.shape) )
                print("valid_node_Y shape: {}".format(valid_node_Y.shape) )
                print("valid_edge_force shape: {}".format(valid_edge_force.shape) )
                print("valid_edge_potential shape: {}".format(valid_edge_potential.shape) )

                print("testing_node_X shape: {}".format(testing_node_X.shape) )
                print("testing_node_Y shape: {}".format(testing_node_Y.shape) )
                print("testing_edge_force shape: {}".format(testing_edge_force.shape) )
                print("testing_edge_potential shape: {}".format(testing_edge_potential.shape) )




                # model configuration
                aggr = 'add'
                hidden = 300
                msg_dim = 1

                # n_f = data.shape[3]
                n_f = 2 * dim + 2 # node position, node velocity, node charge, node mass
                # n_f = dim + 1 # node position, node mass

                # configurate model
                if model_flag == "ours":
                    print("Run OUR model")
                    if act_func == "SiLU":
                        model = GN_potential_ours_SiLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "ReLU":
                        model = GN_potential_ours_ReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "GELU":
                        model = GN_potential_ours_GELU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,   hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "tanh":
                        model = GN_potential_ours_Tanh(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,   hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "sigmoid":
                        model = GN_potential_ours_Sigmoid(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "softplus":
                        model = GN_potential_ours_Softplus(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "LeakyReLU":
                        model = GN_potential_ours_LeakyReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    else:
                        print("act_func flag error")
                        exit()
                    
                elif model_flag == "original":                    
                    if act_func == "SiLU":
                        model = GN_potential_original_SiLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "ReLU":
                        model = GN_potential_original_ReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "GELU":
                        model = GN_potential_original_GELU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "tanh":
                        model = GN_potential_original_Tanh(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "sigmoid":
                        model = GN_potential_original_Sigmoid(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "softplus":
                        model = GN_potential_original_Softplus(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "LeakyReLU":
                        model = GN_potential_original_LeakyReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    else:
                        print("act_func flag error")
                        exit()                    
                    
                else:
                    print("not valide model")
                    exit()



                edge_list = np.transpose(edge_list)
                print("edge_list shape: {}".format(edge_list.shape) )
                print(edge_list.dtype)

                data_example = Data(
                    x = torch.from_numpy(training_node_X[0]),
                    edge_index = torch.tensor(edge_list, dtype=torch.long),
                    y=torch.from_numpy(training_node_Y[0]) )



                batch_size = 1


                training_example_number = len(training_node_X)
                print(edge_list.dtype)
                trainloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(training_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(training_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        node_potential_label = torch.from_numpy(training_node_potential[i]),        
                        edge_force_label = torch.from_numpy(training_edge_force[i]),
                        edge_potential_label = torch.from_numpy(training_edge_potential[i]))  for i in range(training_example_number ) ],
                    batch_size=batch_size,
                    shuffle=True
                )



                valid_example_number = len(valid_node_X)
                validloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(valid_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(valid_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        node_potential_label = torch.from_numpy(valid_node_potential[i] ),
                        edge_force_label = torch.from_numpy(valid_edge_force[i]),
                        edge_potential_label = torch.from_numpy(valid_edge_potential[i]) )  for i in range(valid_example_number) ],
                    batch_size = batch_size,
                    shuffle = False
                    )

                testing_example_number = len(testing_node_X)
                testloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(testing_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(testing_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        node_potential_label = torch.from_numpy(testing_node_potential[i] ),        
                        edge_force_label = torch.from_numpy(testing_edge_force[i]),
                        edge_potential_label = torch.from_numpy(testing_edge_potential[i]) )  for i in range(testing_example_number ) ],
                    batch_size=batch_size,
                    shuffle=False
                )


                total_epochs = 200 # training epochs
                # total_epochs = 100 # training epochs

                model_path = saving_dir + "saved_checkpoint_before_training"
                checkpoint = torch.load(model_path )
                # print(checkpoint.keys()): dict_keys(['model', 'opt', 'lr_sched', 'training_loss', 'valid_loss', 'testing_loss'])

                min_valid_loss = checkpoint["valid_loss"]
                test_loss = 0.0
                train_loss = 0.0
                epoch_id = 0


                for epoch in tqdm(range(0, total_epochs)):
                    model_path = saving_dir + "saved_checkpoint_at_epoch_{}".format(epoch)
                    checkpoint = torch.load(model_path )
                    # model.load_state_dict(checkpoint['model'])

                    this_valid_loss = checkpoint['valid_loss']
                    this_test_loss = checkpoint['testing_loss']
                    if this_valid_loss < min_valid_loss:
                        min_valid_loss = this_valid_loss
                        test_loss = this_test_loss
                        training_loss = checkpoint['training_loss']
                        epoch_id = epoch

                print("\n\n" + "********" * 4)
                print("simulation: {}, NN model: {}, dim: {}".format(sim_type, model_flag, dim))
                print("###Extract trained model")
                print("train_loss: {}, min_valid_loss: {}, test_loss: {}, epoch_id: {}".format(training_loss, min_valid_loss, test_loss, epoch_id) )



                # --------------------------

                # 1. compute the Acceleration Error(predicted_acceleration, true_acceleration)
                def cmpt_loss(input_dataloader, ogn_model):
                    total_loss = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        # edge_feature = ginput.edge_feature.cuda()
                        edge_feature = None
                        # ginput.batch = ginput.batch.cuda() # https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html#mini-batches
                        predicted_y = ogn_model(x, edge_index, edge_feature)
                        if square_flag:
                            loss = torch.sum((true_y - predicted_y) **2 )
                        else:
                            loss = torch.sum(torch.abs(true_y - predicted_y) )
                        total_loss += loss.item()
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch
                    return total_loss / num_items



                #  run the best model, for testing loss and valid loss
                best_model_path = saving_dir + "saved_checkpoint_at_epoch_{}".format(epoch_id)
                best_checkpoint = torch.load(best_model_path )
                model.load_state_dict(best_checkpoint['model'])


                model.eval()
                print("###Acceleration loss")
                valid_acc_loss = cmpt_loss(validloader, model)
                testing_acc_loss = cmpt_loss(testloader, model)
                print("1., {}, Valid Acceleration Loss: {}, Testing Acceleration Loss: {}".format(epoch_id, valid_acc_loss, testing_acc_loss) )


                # --------------------------
                # 2. compute the edge force Error (F = - ∇U)
                def cmpt_edge_force_error(input_dataloader, ogn_model):
                    total_error = 0.0; num_items = 0
                    for ginput in tqdm(input_dataloader):
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None
                        
                        # edge force label
                        edge_force_label = ginput.edge_force_label.cuda()

                        X_feature = x.clone().detach().requires_grad_(True)
                        tmp12 = torch.cat([X_feature[edge_index[0] ], X_feature[edge_index[1] ] ], dim=1)
                        if model_flag == "ours":                        
                            m12 = ogn_model.edge_fnc(tmp12)            
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp12)
                        else:
                            print("IN cmpt_edge_force_error, model_flag error")
                            exit()

                        m12 = torch.squeeze(m12)
                        
                        # print("edge_index[0] length: ")
                        # print(len(edge_index[0] ) )
                        # print("m12 shape")
                        # print(m12.shape)
                        # exit()
                        force_list = []
                        for edge_id in range(len(m12)):
                            v_id = edge_index[0][edge_id]
                            # print(v_id)
                            # print(m12[edge_id])
                            this_force = - torch.autograd.grad(m12[edge_id], X_feature, create_graph=True)[0]
                            # print(this_force)
                            this_force = this_force[v_id][0:data_dim]
                            force_list.append(this_force)
                            # print(this_force)
                            # exit()
                        predicted_edge_force = torch.stack(force_list, dim=0) # the net force on every edge
                            
                        

                        # print("edge_force_label")
                        # print(edge_force_label)
                        # print(edge_force_label.shape)
                        # print("predicted_edge_force")
                        # print(predicted_edge_force)
                        # print(predicted_edge_force.shape)

                        edge_force_label = edge_force_label.detach().cpu()
                        predicted_edge_force = predicted_edge_force.detach().cpu()

                        error = torch.sum(torch.abs(predicted_edge_force - edge_force_label ) )
                        total_error += error.item()

                        num_items += int(len(m12))

                    return total_error / num_items


                model.eval()
                print("###Edge force loss")
                valid_edge_force_loss = cmpt_edge_force_error(validloader, model)
                testing_edge_force_loss = cmpt_edge_force_error(testloader, model)
                print("2., {}, Valid Edge Force Loss: {}, Testing Edge Force Loss: {}".format(epoch_id, valid_edge_force_loss, testing_edge_force_loss) )



                # --------------------------
                # 3. compute the Error on net Node force (predicted_force, true_force)
                def cmpt_node_force_error(input_dataloader, ogn_model):
                    total_error = 0.0; num_items = 0
                    # for ginput in input_dataloader:
                    for ginput in tqdm(input_dataloader):
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None
                        
                        # true force
                        true_force = true_y * x[:, -1][:, None]

                        # predicted force by model
                                
                        if model_flag == "ours":
                            X_feature = x.clone().detach().requires_grad_(True)
                            potential = ogn_model.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)
                            force_list = []
                            for i in range(X_feature.size(0) ):
                                this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
                                this_force = this_force[i][0:ogn_model.ndim] # take out the partitial derivative to position
                                force_list.append(this_force)
                                predicted_force = torch.stack(force_list, dim=0) # the net force on every node
                                # predicted_acc = force / x[:, -1][:, None] # divide the mass

                            # print(potential)
                            # print(potential.shape)
                            # print(predicted_force)
                            # print(predicted_force.shape)
                            # exit()
                        elif model_flag == "original":
                            X_feature = x.clone().detach().requires_grad_(True)
                            tmp = torch.cat([X_feature[edge_index[0] ], X_feature[edge_index[1] ] ], dim=1)
                            m12 = ogn_model.msg_fnc(tmp)   
                            aggragated_message = torch.zeros(X_feature.size(0), msg_dim).cuda()
                            
                            for j in range(edge_index.shape[1]):
                                send_node = edge_index[0][j]
                                aggragated_message[send_node] += m12[j]
                            
                            potential = aggragated_message

                            # predicted_acc = ogn_model.node_fnc(torch.cat([X_feature, aggragated_message], dim=1))

                            force_list = []
                            for i in range(X_feature.size(0) ):
                                this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
                                this_force = this_force[i][0:dim] # take out the partitial derivative to position
                                force_list.append(this_force)
                                predicted_force = torch.stack(force_list, dim=0) # the net force on every node            

                        else:
                            print("IN cmpt_node_force_error, model_flag error")
                            exit()      

                        
                        
                        # print(aggragated_message)
                        # exit()
                        


                        error = torch.sum(torch.abs(true_force - predicted_force) ) # the force error
                        # error = torch.sum((true_force - predicted_force)**2) # the force error, squared

                        # error = torch.sum(torch.abs(predicted_force / x[:, -1][:, None] - true_y) ) # the Acceleration error
                        # error = torch.sum(torch.abs(predicted_acc - true_y) ) # the Acceleration error

                        
                        total_error += error.item()
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch


                    return total_error / num_items



                model.eval()
                print("###Force Error")
                valid_node_force_error = cmpt_node_force_error(validloader, model )
                test_node_force_error = cmpt_node_force_error(testloader, model )
                print("3., {}, Valid Node Force Error: {}, Testing Node Force Error: {}".format(epoch_id, valid_node_force_error, test_node_force_error) )





                # --------------------------
                ## compute the Newton's Third Law error: (P_ij, P_ji)

                # 4. compute the Error of Symmetry potenial on Edges (P_ij, P_ij )
                def cmpt_potential_symmetry(input_dataloader, ogn_model):
                    total_error = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None

                        tmp12 = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)
                        tmp21 = torch.cat([x[edge_index[1] ], x[edge_index[0] ] ], dim=1)        
                        if model_flag == "ours":
                            m12 = ogn_model.edge_fnc(tmp12)
                            m21 = ogn_model.edge_fnc(tmp21)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp12)
                            m21 = ogn_model.msg_fnc(tmp21)
                        else:
                            print("IN cmpt_potential_symmetry, model_flag error")
                            exit()
                        # print("m12 shape")
                        # print(m12.shape) # (56*batch_size, 2)

                        # print("m21 shape")
                        # print(m21.shape)  # (56*batch_size, 2)

                        ## m12 should approximate m21 
                        error = torch.sum(torch.abs(m12 - m21 ) ) # Important: m12 - m21, because the P_ij and P_ji should be same
                        total_error += error.item()
                        num_items += int(m12.shape[0])        
                        
                    return total_error / num_items
                        



                model.eval()
                with torch.no_grad():
                    print("### Error of Symmetry of Edge Potentials (Newton Third Law)")
                    valid_potential_symmetry_error = cmpt_potential_symmetry(validloader, model )
                    test_potential_symmetry_error = cmpt_potential_symmetry(testloader, model )
                    print("4., {}, Valid Potential Symmetry Error: {}, Testing Potential Symmetry Error: {}".format(epoch_id, valid_potential_symmetry_error, test_potential_symmetry_error) )


                




                # --------------------------
                # 5. the change of potential for every edge. First time step is base
                def cmpt_potential_increment_every_edge(input_dataloader, ogn_model):
                    predict_potential_every_edge_dict = {}
                    true_potential_every_edge_dict = {}
                    num_edges = 12 * 11
                    for i in range(num_edges):
                        predict_potential_every_edge_dict[i] = []
                        true_potential_every_edge_dict[i] = []

                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None
                        
                        # true potential
                        edge_potential_label = ginput.edge_potential_label.cuda()

                        # predicted force by model
                        tmp = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)
                        if model_flag == "ours":
                            m12 = ogn_model.edge_fnc(tmp)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp)   
                        else:
                            print("IN cmpt_force_error, model_flag error")
                            exit()         
                        

                        assert len(m12) == num_edges
                        assert len(edge_potential_label) == num_edges
                        

                        m12 = m12.detach().cpu().numpy()
                        edge_potential_label = edge_potential_label.detach().cpu().numpy()

                        for i in range(num_edges):
                            predict_potential_every_edge_dict[i].append(m12[i, 0] )
                            true_potential_every_edge_dict[i].append(edge_potential_label[i, 0] )

                    # potential_every_edge_dict = {
                    #     "predict_potential_every_edge_dict": predict_potential_every_edge_dict,
                    #     "true_potential_every_edge_dict": true_potential_every_edge_dict
                    # }



                    total_error = 0.0
                    num_items = 0
                    for edge_id in range(len(true_potential_every_edge_dict)):
                        true_potential_this_edge = true_potential_every_edge_dict[edge_id]
                        predict_potential_this_edge = predict_potential_every_edge_dict[edge_id]

                        for i in range(1, len(true_potential_this_edge) ):
                            true_potential_difference = true_potential_this_edge[i] - true_potential_this_edge[0]
                            predict_potential_difference = predict_potential_this_edge[i] - predict_potential_this_edge[0]
                            total_error += np.abs(true_potential_difference - predict_potential_difference)
                            num_items += 1


                    # print("{}".format(total_error / num_items) )
                    # print("{}".format(num_items) )

                    return total_error / num_items, num_items



                model.eval()
                with torch.no_grad():
                    print("###Edge Potential Increment Error (compared with first time step in test)")
                    valid_edge_potential_increment_error, _ = cmpt_potential_increment_every_edge(validloader, model )
                    test_edge_potential_increment_error, _ = cmpt_potential_increment_every_edge(testloader, model )
                    print("5., {}, valid_edge_potential_increment_error: {}, test_edge_potential_increment_error: {}".format(epoch_id, valid_edge_potential_increment_error, test_edge_potential_increment_error) )



                # --------------------------
                # 6. the change of potential for every node. First time step is base
                def cmpt_potential_increment_every_node(input_dataloader, ogn_model):
                    predict_potential_every_node_dict = {}
                    true_potential_every_node_dict = {}
                    assert num_nodes == 12
                    for i in range(num_nodes):
                        predict_potential_every_node_dict[i] = []
                        true_potential_every_node_dict[i] = []

                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None
                        
                        # true potential
                        true_node_potential = ginput.node_potential_label.cuda()

                        # predicted force by model
                        tmp = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)
                        if model_flag == "ours":
                            m12 = ogn_model.edge_fnc(tmp)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp)   
                        else:
                            print("IN cmpt_force_error, model_flag error")
                            exit()

                        assert x.size(0) == num_nodes
                        aggragated_message = torch.zeros(x.size(0), 1).cuda()
                        for j in range(edge_index.shape[1]):
                            send_node = edge_index[0][j]
                            aggragated_message[send_node] += m12[j]


                        predicted_node_potential = aggragated_message.detach().cpu().numpy()
                        true_node_potential = true_node_potential.detach().cpu().numpy()

                        for i in range(num_nodes):
                            predict_potential_every_node_dict[i].append(predicted_node_potential[i, 0] )
                            true_potential_every_node_dict[i].append(true_node_potential[i, 0] )

                    # cmpt the error
                    total_error = 0.0
                    num_items = 0

                    for node_id in range(len(predict_potential_every_node_dict)):
                        true_potential_this_node = true_potential_every_node_dict[node_id]
                        predict_potential_this_node = predict_potential_every_node_dict[node_id]

                        # all time steps, except the first time step which is the base case
                        for i in range(1, len(true_potential_this_node) ):
                            true_potential_difference = true_potential_this_node[i] - true_potential_this_node[0]
                            predict_potential_difference = predict_potential_this_node[i] - predict_potential_this_node[0]
                            total_error += np.abs(true_potential_difference - predict_potential_difference)
                            num_items += 1

                    return total_error / num_items, num_items


                model.eval()
                with torch.no_grad():
                    print("###Node Potential Increment Error (compared with first time step in test)")
                    valid_node_potential_increment_error, _ = cmpt_potential_increment_every_node(validloader, model )
                    test_node_potential_increment_error, _ = cmpt_potential_increment_every_node(testloader, model )
                    print("6., {}, valid_node_potential_increment_error: {}, test_node_potential_increment_error: {}".format(epoch_id, valid_node_potential_increment_error, test_node_potential_increment_error) )






                

# --------------------------

# 1. Acceleration loss 
# 2. Edge force Error (F = - ∇U)                
# 3. Net force error on nodes 
# 4. Symmetry of potential on edges
# 5. The potential increment error for every edge
# 6. The potential increment error for every node

                