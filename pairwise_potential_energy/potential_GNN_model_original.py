'''
The baseline model implementation is from: 
https://github.com/MilesCranmer/symbolic_deep_learning/blob/master/models.py (MIT license)
'''


import numpy as np
import torch
from torch import nn
from torch.functional import F
from torch.optim import Adam
from torch_geometric.nn import MetaLayer, MessagePassing
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, GELU, Tanh, Softplus, Sigmoid, LeakyReLU
from torch.autograd import Variable, grad



torch.set_default_dtype(torch.float64)


class SiLU(torch.nn.Module):    
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str


# the baseline model, with SiLU activation functions
class GN_potential_original_SiLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_SiLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            SiLU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with ReLU activation functions (default)
class GN_potential_original_ReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_ReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            ReLU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with GELU activation functions
class GN_potential_original_GELU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_GELU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            GELU(),
            Lin(hidden, hidden),
            GELU(),
            Lin(hidden, hidden),
            GELU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            GELU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            GELU(),
            Lin(hidden, hidden),
            GELU(),
            Lin(hidden, hidden),
            GELU(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with Tanh activation functions
class GN_potential_original_Tanh(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_Tanh, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            Tanh(),
            Lin(hidden, hidden),
            Tanh(),
            Lin(hidden, hidden),
            Tanh(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Tanh(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            Tanh(),
            Lin(hidden, hidden),
            Tanh(),
            Lin(hidden, hidden),
            Tanh(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with Softplus activation functions
class GN_potential_original_Softplus(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_Softplus, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            Softplus(),
            Lin(hidden, hidden),
            Softplus(),
            Lin(hidden, hidden),
            Softplus(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Softplus(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            Softplus(),
            Lin(hidden, hidden),
            Softplus(),
            Lin(hidden, hidden),
            Softplus(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with Sigmoid activation functions
class GN_potential_original_Sigmoid(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_Sigmoid, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            Sigmoid(),
            Lin(hidden, hidden),
            Sigmoid(),
            Lin(hidden, hidden),
            Sigmoid(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Sigmoid(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            Sigmoid(),
            Lin(hidden, hidden),
            Sigmoid(),
            Lin(hidden, hidden),
            Sigmoid(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]


# the baseline model, with LeakyReLU activation functions
class GN_potential_original_LeakyReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_original_LeakyReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        assert msg_dim == 1 # the potential is a scalar
        torch.set_default_dtype(torch.float64)
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            LeakyReLU(),
            Lin(hidden, hidden),
            LeakyReLU(),
            Lin(hidden, hidden),
            LeakyReLU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            LeakyReLU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            LeakyReLU(),
            Lin(hidden, hidden),
            LeakyReLU(),
            Lin(hidden, hidden),
            LeakyReLU(),
            Lin(hidden, ndim)
        )    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]












