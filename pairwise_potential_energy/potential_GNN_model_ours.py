'''
The PIGNPI model to learn pairwise potential energy, with different activation functions
'''

import numpy as np
import torch
from torch import nn
from torch.functional import F
from torch.optim import Adam
from torch_geometric.nn import MetaLayer, MessagePassing
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, GELU, Tanh, Softplus, Sigmoid, LeakyReLU
from torch.autograd import Variable, grad



torch.set_default_dtype(torch.float64)


class SiLU(torch.nn.Module):    
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str






# with SiLU
class GN_potential_ours_SiLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_SiLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_SiLU, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            SiLU(),

            Lin(hidden, hidden),            
            SiLU(),

            Lin(hidden, hidden),
            SiLU(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            SiLU(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_SiLU, activation function is: {}".format( "SiLU" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        






# with ReLU
class GN_potential_ours_ReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_ReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_ReLU, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            ReLU(),

            Lin(hidden, hidden),            
            ReLU(),

            Lin(hidden, hidden),
            ReLU(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            ReLU(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_ReLU, activation function is: {}".format( "ReLU" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        

# with GELU
class GN_potential_ours_GELU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_GELU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_GELU, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            GELU(),

            Lin(hidden, hidden),            
            GELU(),

            Lin(hidden, hidden),
            GELU(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            GELU(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_GELU, activation function is: {}".format( "GELU" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        

# with Tanh
class GN_potential_ours_Tanh(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_Tanh, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_Tanh, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            Tanh(),

            Lin(hidden, hidden),            
            Tanh(),

            Lin(hidden, hidden),
            Tanh(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Tanh(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_Tanh, activation function is: {}".format( "Tanh" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        

# with Softplus
class GN_potential_ours_Softplus(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_Softplus, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_Softplus, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            Softplus(),

            Lin(hidden, hidden),            
            Softplus(),

            Lin(hidden, hidden),
            Softplus(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Softplus(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_Softplus, activation function is: {}".format( "Softplus" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        



# with Sigmoid
class GN_potential_ours_Sigmoid(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_Sigmoid, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_Sigmoid, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            Sigmoid(),

            Lin(hidden, hidden),            
            Sigmoid(),

            Lin(hidden, hidden),
            Sigmoid(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Sigmoid(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_Sigmoid, activation function is: {}".format( "Sigmoid" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        



# with LeakyReLU
class GN_potential_ours_LeakyReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_potential_ours_LeakyReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        
        assert msg_dim == 1 # the potential is a scalar

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("### In GN_potential_ours_LeakyReLU, n_node: {}, dim: {}".format(self.n_node, self.ndim ) )

        self.edge_fnc = Seq(
            Lin(2*n_f , hidden),            
            LeakyReLU(),

            Lin(hidden, hidden),            
            LeakyReLU(),

            Lin(hidden, hidden),
            LeakyReLU(),
                                 
            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            LeakyReLU(),
            
            Lin(hidden, msg_dim)
        )
        print("### In GN_potential_ours_LeakyReLU, activation function is: {}".format( "LeakyReLU" ) )

                
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        # from https://github.com/xiaulinhu/pinn-pytorch/blob/ac75505ecd7e1121ee622ca43dd9a290a4320c40/grad/pytorchGrad.py#L110
        X_feature = x.clone().detach().requires_grad_(True)
        
        # compute the potential for every node
        potential = self.propagate(edge_index, size=(X_feature.size(0), X_feature.size(0)), x=X_feature, edge_feature_list=edge_feature)

        force_list = []
        for i in range(X_feature.size(0) ):
            # pay attention to the minus
            this_force = - torch.autograd.grad(potential[i], X_feature, create_graph=True)[0]  # partitial derivative to input feature
            
            this_force = this_force[i][0:self.ndim] # take out the partitial derivative to position
            
            force_list.append(this_force)
            # exit()

        # print(force_list)
        force = torch.stack(force_list, dim=0)
        
        predicted_acc = force / x[:, -1][:, None] # divide the mass

        return predicted_acc         
        # return out / x[:, -1][:, None] # add edges from all types, then divide the mass
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] ], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
            # tmp = torch.cat([x_i[:, 0:self.ndim ], x_j[:, 0:self.ndim ] , edge_feature_list], dim=1)        
        return self.edge_fnc(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # x shape: [n_node*batch_size, nf]        
        # tmp = torch.cat([x, aggr_out], dim=1) # tmp shape: [n_node*batch_size, nf+msg_dim]

        # potential = self.node_fnc(tmp) # the potential at every node: [n_node*batch_size, 1]  
        # potential = torch.squeeze(potential)
        potential = torch.squeeze(aggr_out)
        return potential      
        



























