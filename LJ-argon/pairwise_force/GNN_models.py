'''
The implementation of PIGNPI, baseline and GN+ (lemos2021rediscovering)
'''

import numpy as np
import torch
import torch.nn as nn
from torch.functional import F
from torch.optim import Adam
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, GELU, Tanh, Softplus, Sigmoid, LeakyReLU
from torch.autograd import Variable, grad

torch.set_default_dtype(torch.float64)



class SiLU(nn.Module):    
    # from https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#SiLU, since it is missing in 1.6.0
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str



class PIGNPI_force(nn.Module):
    def __init__(self, n_node, input_dim, msg_dim, hidden_dim):
        super(PIGNPI_force, self).__init__()

        self.num_node = n_node

        assert input_dim == 7 #  7: pos, vel, masses

        input_dim = 2 * input_dim + 3 # x, y, z, vx, vy, vz, dx, dy, dz

        self.msg_fnc = Seq(
            Lin(input_dim, hidden_dim), # pos, vel; for two nodes
            SiLU(),

            Lin(hidden_dim, hidden_dim),
            SiLU(),

            Lin(hidden_dim, hidden_dim),
            SiLU(),

            #(Can turn on or off this layer:)
            Lin(hidden_dim, hidden_dim), 
            SiLU(),
            
            Lin(hidden_dim, msg_dim)
        )
        self.msg_fnc.apply(self.init_weights)

    def init_weights(self, m):
        if isinstance(m, nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


    def cmpt_net_force(self, inputs, edge_feature, rel_rec, rel_send):
        time_steps, num_atoms, num_dims = inputs.shape
        num_timesteps, num_edges, num_particles = rel_rec.shape

        receiver_input_state = torch.bmm(rel_rec, inputs) 
        sender_input_state = torch.bmm(rel_send, inputs)

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)

        edge_msg = self.msg_fnc(pre_msg)#; print(edge_msg.shape); exit()

        rec_edge_mat = rel_rec.transpose(-2, -1)
        agg_msgs = torch.bmm(rec_edge_mat, edge_msg)

        return agg_msgs

       





class Baseline_force(nn.Module):
    def __init__(self, n_node, input_dim, msg_dim, hidden_dim):
        super(Baseline_force, self).__init__()

        self.num_node = n_node
        assert input_dim == 7 #  7: pos, vel, masses

        self.msg_fnc = Seq(
            Lin(2 * input_dim + 3, hidden_dim), # pos, vel, charge, mass; for two nodes
            ReLU(), # baseline uses ReLU as default
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            #(Can turn on or off this layer:)
            Lin(hidden_dim, hidden_dim), 
            ReLU(),
            Lin(hidden_dim, msg_dim)
        )

        self.node_fnc = Seq(
            Lin(msg_dim+input_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, 3) # the output dimension is 3
        )
        self.msg_fnc.apply(self.init_weights)
        self.node_fnc.apply(self.init_weights)

    def init_weights(self, m):
        if isinstance(m, nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


    def cmpt_net_force(self, inputs, edge_feature, rel_rec, rel_send):
        time_steps, num_atoms, num_dims = inputs.shape

        receiver_input_state = torch.bmm(rel_rec, inputs) 
        sender_input_state = torch.bmm(rel_send, inputs)
        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)

        edge_msg = self.msg_fnc(pre_msg)#; print(edge_msg.shape); exit()

        rec_edge_mat = rel_rec.transpose(-2, -1)
        agg_msgs = torch.bmm(rec_edge_mat, edge_msg)

        combined_node_edge = torch.cat([inputs, agg_msgs], dim = -1) # combine node feature and in-coming edge features
        prediction = self.node_fnc(combined_node_edge)
        return prediction




class lemos2021rediscovering(nn.Module):
    def __init__(self, n_node, input_dim, msg_dim, hidden_dim, same_mass):
        '''
        Default same_mass: False. In paper, assign each node an individual learnable scalar
        '''
        super(lemos2021rediscovering, self).__init__()

        self.num_node = n_node

        assert input_dim == 7 #  7: pos, vel, masses

        input_dim = 2 * input_dim + 3 # x, y, z, vx, vy, vz, dx, dy, dz

        ## assign a learnable scalar to each node (v_i in the paper)
        self.same_mass = same_mass
        print("### In lemos2021rediscovering, same_mass flag: ", self.same_mass)
        if self.same_mass == False:
            self.W = torch.nn.Parameter(data=torch.rand(self.num_node, 1) * (-2) + 1, requires_grad=True ) # initialize to (-1, 1)        
        else:
            self.W = torch.nn.Parameter(data=torch.rand(1) * (-2) + 1, requires_grad=True ) # initialize to (-1, 1)        


        self.msg_fnc = Seq(
            Lin(input_dim, hidden_dim), # pos, vel; for two nodes
            Tanh(),

            Lin(hidden_dim, hidden_dim),
            Tanh(),

            Lin(hidden_dim, hidden_dim),
            Tanh(),

            #(Can turn on or off this layer:)
            Lin(hidden_dim, hidden_dim), 
            Tanh(),
            
            Lin(hidden_dim, msg_dim)
        )
        self.msg_fnc.apply(self.init_weights)

    def init_weights(self, m):
        if isinstance(m, nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


    def cmpt_net_force(self, inputs, edge_feature, rel_rec, rel_send):
        time_steps, num_atoms, num_dims = inputs.shape
        num_timesteps, num_edges, num_particles = rel_rec.shape

        receiver_input_state = torch.bmm(rel_rec, inputs) 
        sender_input_state = torch.bmm(rel_send, inputs)

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)

        edge_msg = self.msg_fnc(pre_msg)#; print(edge_msg.shape); exit()

        rec_edge_mat = rel_rec.transpose(-2, -1)
        agg_msgs = torch.bmm(rec_edge_mat, edge_msg)

        if self.same_mass == False:
            learnable_scalar = [self.W] * time_steps
            learnable_scalar = torch.stack(learnable_scalar, dim=0)
        else:
            learnable_scalar = self.W

        predicted_acc = agg_msgs / (10 ** learnable_scalar)

        return predicted_acc

    

