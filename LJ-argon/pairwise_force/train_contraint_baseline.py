'''
The script to train contraint-baseline
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import numpy as np
import torch
torch.set_num_threads(2)

import pickle as pkl

from torch.optim import Adam

from tqdm import tqdm
import math
import argparse


from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR


from GNN_models import Baseline_force
from utils import load_data_xyzvxvyvz_dxdydz # load_data_xyzvxvyvz_dxdydz, load_data_dxdydz, load_data_xyz_dxdydz




parser = argparse.ArgumentParser()
parser.add_argument('--no_cuda', action='store_true', default=False, help='Disables CUDA training.')
parser.add_argument('--space_dim', type=int, default=3, help='3 for the LJ-Argon.')
parser.add_argument('--batch_size', type=int, default=16, help='Batch size.')
parser.add_argument('--num_nodes', type=int, default=258, help='Number of nodes.')
parser.add_argument('--input_dim', type=int, default=7, help='node position and node velocity and (unit) mass.')
parser.add_argument('--hidden_dim', type=int, default=128, help='width of hidden layers.')
parser.add_argument('--init_lr', type=float, default=1e-3, help='Initial learning rate.')
parser.add_argument('--model', type=str, default='baseline', help='Type of decoder model (pignpi or baseline).')
parser.add_argument('--epochs', type=int, default=200, help='Number of epochs to train.')
parser.add_argument('--num_train', type=int, default=7000, help='Number of time steps for training.')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()


print("args")
print(args)

torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss


for exp_id in [1, 2, 3, 4, 5]:
    for model_flag in [args.model ]: # "pignpi" for PIGNPI; or "baseline" for the baseline model.
        for ALPHA in [0.1, 1.0, 10, 100]:
            assert model_flag == "baseline"

            if model_flag == "pignpi":           
                print("Not implement yet"); exit()
            elif model_flag == "baseline":
                ## pos + vel + dxdydz
                saving_dir = "./{}/exp{}_trained_model_{}/baseline_xyzvxvyvz_dxdydz_{}epoch_alpha{}/".format("force", exp_id, "LJ-Argon", args.epochs, ALPHA)
                print("--------\nEXP {} LJ-Argon, Train baseline model, to save in {}".format(exp_id, saving_dir) )
            else:
                print("model_flag error")
                exit()
            # assert not os.path.isdir(saving_dir)
            if os.path.isdir(saving_dir):
                import shutil
                shutil.rmtree(saving_dir)

            if not os.path.isdir(saving_dir):                
                from pathlib import Path
                Path(saving_dir).mkdir(parents=True, exist_ok=False)

        

            ## load simulation data
            simulation_data_dir = "../dataset/GNN_input/"
            train_loader = load_data_xyzvxvyvz_dxdydz(simulation_data_dir, args.batch_size, saving_dir)
            
            
            # model
            if model_flag == "pignpi":
                print("not suitable")
                exit()
                
            elif model_flag == "baseline":
                model = Baseline_force(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = args.space_dim, hidden_dim = args.hidden_dim)
                if args.cuda:
                    model = model.cuda()
                else:
                    model = model.cpu()

            else:
                print("not valide model")
                exit()


            # training
            
            opt = torch.optim.Adam(model.parameters(), lr=args.init_lr, weight_decay=1e-8)

            training_example_number = args.num_train
            print('training_example_number: {}'.format(training_example_number))

            batch_per_epoch = math.ceil(training_example_number / args.batch_size )

            sched = OneCycleLR(opt, max_lr=args.init_lr,
                               steps_per_epoch=batch_per_epoch,#len(train_loader),
                               epochs=args.epochs, final_div_factor=1e5)

            print("batch_per_epoch: {}".format(batch_per_epoch))


            def cmpt_loss(input_dataloader, model):
                # compute the acceleration loss 
                loss_sum = 0.0; num_items = 0
                for batch_idx, (feat, acc, edge_feature, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, _) in tqdm(enumerate(input_dataloader)):
                    if feat is not None:
                        feat = feat.cuda()
                    acc = acc.cuda()
                    edge_feature = edge_feature.cuda()

                    num_graph, max_edge_number, _ = edge_feature.shape
                    sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
                    rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                    edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                    edge_rec_mat = edge_rec_mat.cuda()
                    rec_indices = None

                    send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                    edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                    edge_send_mat = edge_send_mat.cuda()
                    send_indices = None

                    predicted_acc = model.cmpt_net_force(feat, edge_feature, edge_rec_mat, edge_send_mat)                
                    loss = torch.sum(torch.abs(predicted_acc - acc) )
                    loss_sum += loss.item()
                    num_items += len(acc) * args.num_nodes # all the nodes number in this batch
                return loss_sum / num_items

            def cmpt_regularization(feat, edge_feature, edge_rec_mat, edge_send_mat, num_edges_list_tensor, model):
                num_graph, max_edge_number, _ = edge_feature.shape

                sender_input_state = torch.bmm(edge_send_mat, feat)
                receiver_input_state = torch.bmm(edge_rec_mat, feat)
                pre_msg12 = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
                pre_msg21 = torch.cat([receiver_input_state, sender_input_state, - edge_feature], dim=-1) # negative edge_feature
                m12 = model.msg_fnc(pre_msg12)
                m21 = model.msg_fnc(pre_msg21)


                mask = torch.arange(max_edge_number).expand(len(num_edges_list_tensor), max_edge_number) < num_edges_list_tensor.unsqueeze(1)
                mask = mask.int().float()
                mask = mask[:, :, None] # expand the dimension, for broadcase
                mask = mask.cuda()

                m12 = m12 * mask
                m21 = m21 * mask

                constraint_error = torch.sum(torch.abs(m12 + m21 ) ) 
                return constraint_error


            ############# before training
            print("Before training")
            initial_training_loss = cmpt_loss(train_loader, model)
            print("Initial Training Loss: {}".format(initial_training_loss))

            saving_path = saving_dir + "saved_checkpoint_before_training"
            checkpoint = { 
                'model': model.state_dict(),
                'opt': opt.state_dict(),
                'lr_sched': sched,
                'training_loss': initial_training_loss,
            }
            print("saving untrained model at {}".format(saving_path))
            torch.save(checkpoint, saving_path)


            for epoch in tqdm(range(0, args.epochs)):
                training_loss_sum = 0.0
                num_nodes = 0
                training_error = []
                for batch_idx, (feat_train, acc_train, edge_feature_train, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, num_edges_list_tensor) in enumerate(train_loader):
                    if args.cuda:
                        if feat_train is not None:
                            feat_train = feat_train.cuda()
                        acc_train = acc_train.cuda()
                        edge_feature_train = edge_feature_train.cuda()
                        

                    num_graph, max_edge_number, _ = edge_feature_train.shape
                    sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
                    rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                    edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                    edge_rec_mat = edge_rec_mat.cuda()
                    rec_indices = None

                    send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                    edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                    edge_send_mat = edge_send_mat.cuda()
                    send_indices = None


                    opt.zero_grad()           
                    # Attention: use cmpt_net_force, ground-truth is net force !!
                    predicted_acc = model.cmpt_net_force(feat_train, edge_feature_train, edge_rec_mat, edge_send_mat)


                    loss = torch.sum(torch.abs(acc_train - predicted_acc) )

                    # loss and regularization
                    loss_and_regularization = loss / (num_graph * args.num_nodes)  + ALPHA * cmpt_regularization(feat_train, edge_feature_train, edge_rec_mat, edge_send_mat, num_edges_list_tensor, model) / num_edges_list_tensor.sum().item()

                    loss_and_regularization.backward()
                    opt.step()
                    sched.step()
                    training_loss_sum += loss.item()
                    training_error.append(loss_and_regularization.item())

                    num_nodes += len(acc_train) * args.num_nodes # the number of nodes in this batch

                training_loss = training_loss_sum / num_nodes

                print("Epoch: {}, training loss: {}, with regularization: {}".format(epoch, training_loss, np.mean(training_error)))

                # save the model current epoch
                saving_path = saving_dir + "checkpoint_{}.pt".format(epoch)
                checkpoint = { 
                    'epoch': epoch,
                    'model': model.state_dict(),
                    'opt': opt.state_dict(),
                    'lr_sched': sched,
                    'training_loss': training_loss,
                }
                torch.save(checkpoint, saving_path)


                
