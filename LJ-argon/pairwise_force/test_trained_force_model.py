'''
The script to test trained models
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
import torch
from torch.autograd import Variable
from matplotlib import pyplot as plt
import pickle as pkl
from torch.optim import Adam
from tqdm import tqdm
import math
import argparse

torch.set_num_threads(2)
torch.set_printoptions(precision=10)

from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR

from GNN_models import PIGNPI_force, Baseline_force
from GNN_models import lemos2021rediscovering

from utils_test import load_data_test



parser = argparse.ArgumentParser()
parser.add_argument('--no_cuda', action='store_true', default=False, help='Disables CUDA training.')
parser.add_argument('--space_dim', type=int, default=3, help='3 for the LJ-Argon.')
parser.add_argument('--batch_size', type=int, default=64, help='Batch size.')
parser.add_argument('--num_nodes', type=int, default=258, help='Number of nodes.')
parser.add_argument('--input_dim', type=int, default=7, help='2 * args.space_dim + 1 # node position, node velocity, node mass.')
parser.add_argument('--hidden_dim', type=int, default=128, help='width of hidden layers.')
parser.add_argument('--init_lr', type=float, default=1e-3, help='Initial learning rate.')
parser.add_argument('--epochs', type=int, default=500, help='Number of epochs to train.')
parser.add_argument('--num_train', type=int, default=7000, help='Number of time steps for training.')
parser.add_argument('--num_valid', type=int, default=1500, help='Number of simulation steps used for validation.')
parser.add_argument('--num_test', type=int, default=1500, help='Number of simulation steps used for testing.')
parser.add_argument('--task_type', type=str, default="force", help='learn potential [potential] or force [force]')
parser.add_argument('--model_flag', type=str, default="pignpi", help='choose the model [pignpi] or [baseline] or [lemos2021rediscovering]')
parser.add_argument('--lemos_same_mass', type=str, default='True', help='Whether assign individual scalar to nodes.')
# parser.add_argument('--box_size', type=float, default=-1, help='box size (-1.0 or 27.27064966799712)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
assert args.task_type == "force" # test the force models
assert args.lemos_same_mass == "True"
assert args.input_dim == 7
print(args)

torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss



def cmpt_acc_loss(input_dataloader, model):
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    for batch_idx, (feat, acc, edge_feature, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        feat = feat.cuda()
        edge_feature = edge_feature.cuda()
        acc = acc.cuda()
        num_graph, max_edge_number, _ = edge_feature.shape
        sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        predicted_acc = model.cmpt_net_force(feat, edge_feature, edge_rec_mat, edge_send_mat)                
        loss = torch.sum(torch.abs(predicted_acc - acc) )
        total_loss += loss.item()
        num_items += len(feat) * 258 # all the nodes number in this batch
        ground_truth_magnitude += torch.sum(torch.abs(acc) ).item() # the magnitude of ground-truth acc
    return total_loss / num_items, total_loss / ground_truth_magnitude # abs loss, relative loss



def cmpt_pairwise_force_loss(input_dataloader, model):
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    for batch_idx, (feat, acc, edge_feature, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        max_len = pairwise_force.shape[1] # max edge numbers in this batch
        feat = feat.cuda()
        edge_feature = edge_feature.cuda()

        num_graph, max_edge_number, _ = edge_feature.shape
        sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        pairwise_force = pairwise_force.cuda()

        sender_input_state = torch.bmm(edge_send_mat, feat)
        receiver_input_state = torch.bmm(edge_rec_mat, feat)                 
        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)

        edge_msg = model.msg_fnc(pre_msg)
        predicted_edge_force = edge_msg
        predicted_edge_force = predicted_edge_force * torch.sqrt(output_var) # denormalize the output
        
        ## NEED to consider the number of "efficient edges" in each graph
        mask = torch.arange(max_len).expand(len(num_edges_list_tensor), max_len) < num_edges_list_tensor.unsqueeze(1)
        mask = mask.int().float()
        mask = mask[:, :, None] # expand the dimension, for broadcase
        mask = mask.cuda()

        predicted_edge_force = predicted_edge_force * mask
        pairwise_force = pairwise_force * mask # only consider the efficient edges

        

        error = torch.sum(torch.abs(predicted_edge_force -  pairwise_force ) )
        total_loss += error.item()
        num_items += num_edges_list_tensor.sum().item()
        ground_truth_magnitude += torch.sum(torch.abs(pairwise_force ) ).item()
    return total_loss / num_items, total_loss / ground_truth_magnitude # abs loss, relative loss




def cmpt_force_symmetry(input_dataloader, model):
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    for batch_idx, (feat, acc, edge_feature, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        max_len = pairwise_force.shape[1]
        feat = feat.cuda()
        edge_feature = edge_feature.cuda()

        num_graph, max_edge_number, _ = edge_feature.shape
        sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        sender_input_state = torch.bmm(edge_send_mat, feat)
        receiver_input_state = torch.bmm(edge_rec_mat, feat)
        pre_msg12 = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        pre_msg21 = torch.cat([receiver_input_state, sender_input_state, - edge_feature], dim=-1) # negative edge_feature
        m12 = model.msg_fnc(pre_msg12)
        m21 = model.msg_fnc(pre_msg21)
        m12 = m12 * torch.sqrt(output_var) # denormalize the output
        m21 = m21 * torch.sqrt(output_var)

        mask = torch.arange(max_len).expand(len(num_edges_list_tensor), max_len) < num_edges_list_tensor.unsqueeze(1)
        mask = mask.int().float()
        mask = mask[:, :, None] # expand the dimension, for broadcase
        mask = mask.cuda()

        m12 = m12 * mask
        m21 = m21 * mask

        error = torch.sum(torch.abs(m12 + m21 ) )
        total_loss += error.item()
        num_items += num_edges_list_tensor.sum().item()

        pairwise_force = pairwise_force.cuda()
        pairwise_force = pairwise_force * mask
        ground_truth_magnitude += torch.sum(torch.abs(pairwise_force)).item()
    return total_loss / num_items, total_loss / ground_truth_magnitude  # abs loss, relative loss



for exp_id in [1, 2, 3, 4, 5]: # should be same as as train_force_main.py

    if args.model_flag == "pignpi":
        saving_dir = "./{}/exp{}_trained_model_{}/PIGNPI_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.epochs)
        print("test the trained pignpi-force, saved in: ", saving_dir)
    elif args.model_flag == "baseline":                    
        saving_dir = "./{}/exp{}_trained_model_{}/baseline_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.epochs)
        print("test the baseline model, saved in: ", saving_dir )
    elif args.model_flag == "lemos2021rediscovering":
        if args.lemos_same_mass == "False":
            saving_dir = "./{}/exp{}_trained_model_{}/lemos2021rediscovering_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.epochs)
        else:
            saving_dir = "./{}/exp{}_trained_model_{}/{}_xyzvxvyvz_dxdydz_sameMass_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.model_flag, args.epochs)
        print("test the lemos2021rediscovering model, saved in: ", saving_dir)
    else:
        print("args.model_flag error")
        exit()

    # trained model should exist
    assert os.path.isdir(saving_dir)


    # output_dir = "./evaluate_trained_model_{}_{}epoch/exp{}/".format(args.model_flag, args.epochs, exp_id)
    if args.model_flag == "pignpi" or args.model_flag == "baseline" or args.lemos_same_mass == "False":
        output_dir = "./evaluate_force_model/{}_{}epoch/".format(args.model_flag, args.epochs)
    else:
        output_dir = "./evaluate_force_model/{}_sameMass_{}epoch/".format(args.model_flag, args.epochs)
    if not os.path.isdir(output_dir):                
        from pathlib import Path
        Path(output_dir).mkdir(parents=True, exist_ok=False)


    with open(saving_dir + "normalization_info.pkl", "rb") as f:
        [input_normalization, target_normalization] = pkl.load(f)
    print("input normalization")
    print(input_normalization)
    print("target normalization")
    print(target_normalization)
    output_var = target_normalization["var"]
    output_var = torch.tensor(output_var).cuda()
    
    


    ## load simulation data
    simulation_data_dir = "../dataset/GNN_input/"

    print("loading validation data")
    valid_loader = load_data_test(simulation_data_dir, args.batch_size, saving_dir, "valid", "force")
    print("loading testing data")
    test_loader = load_data_test(simulation_data_dir, args.batch_size, saving_dir, "test", "force")

    
    # model
    if args.model_flag == "pignpi":
        model = PIGNPI_force(n_node = args.num_nodes, input_dim=args.input_dim, msg_dim=args.space_dim, hidden_dim=args.hidden_dim)
        if args.cuda:
            model = model.cuda()
        else:
            model = model.cpu()
        
    elif args.model_flag == "baseline":
        model = Baseline_force(n_node = args.num_nodes, input_dim=args.input_dim, msg_dim=args.space_dim, hidden_dim=args.hidden_dim)
        if args.cuda:
            model = model.cuda()
        else:
            model = model.cpu()
        # print("not valide model")
        # exit()
    elif args.model_flag == "lemos2021rediscovering":
        if args.lemos_same_mass == "False":
            lemos_same_mass_flag = False
        else:
            lemos_same_mass_flag = True
        model = lemos2021rediscovering(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = args.space_dim, hidden_dim = args.hidden_dim, same_mass = lemos_same_mass_flag)
        if args.cuda:
            model = model.cuda()
        else:
            model = model.cpu()
    else:
        print("not valide model")
        exit()



    ########################################################
    ########################################################
    ########################################################
    ## training loss
    training_loss_list = []
    checkpoint_path = saving_dir + "saved_checkpoint_before_training"
    checkpoint = torch.load(checkpoint_path)
    training_loss_list.append(checkpoint['training_loss'])
    for epoch in tqdm(range(0, args.epochs)):
        checkpoint_path = saving_dir + "checkpoint_{}.pt".format(epoch)
        checkpoint = torch.load(checkpoint_path)
        training_loss_list.append(checkpoint['training_loss'])
        
    training_loss_list = np.array(training_loss_list)
    
    np.savetxt(output_dir + "training_loss_{}.txt".format(exp_id), training_loss_list)



    # '''
    epoch_id_file = output_dir + "exp{}_epoch_id.txt".format(exp_id)

    if not os.path.exists(epoch_id_file):
        print(epoch_id_file + " NOT exist")
        print("load the best model from {} epochs".format(args.epochs))
        ########################################################
        ########################################################
        ########################################################
        ### extract and test the best model
        min_valid_loss = 10000
        relative_valid_acc_loss = -1
        epoch_id = -1 # the epoch_id of best trained model
        checkpoint_path = saving_dir + "saved_checkpoint_before_training"
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint['model'])
        model.eval()
        with torch.no_grad():
            valid_acc_loss, relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
            min_valid_loss = valid_acc_loss

        for epoch in tqdm(range(0, args.epochs)):
            checkpoint_path = saving_dir + "checkpoint_{}.pt".format(epoch)
            checkpoint = torch.load(checkpoint_path)
            model.load_state_dict(checkpoint['model'])
            
            model.eval()
            with torch.no_grad():
                valid_acc_loss, relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
                if min_valid_loss > valid_acc_loss:
                    min_valid_loss = valid_acc_loss
                    epoch_id = epoch

        print("Exp {}, best trained model is at epoch {}, with valid acc loss {}, relative_valid_acc_loss {}".format(exp_id, epoch_id, min_valid_loss, relative_valid_acc_loss) )

        f = open(output_dir + "results_{}.txt".format(exp_id), "w")
        f.write("Exp {}, best trained model is at epoch {}, with valid acc loss {}, relative_valid_acc_loss {}\n".format(exp_id, epoch_id, min_valid_loss, relative_valid_acc_loss))
    else:
        print("epoch_id info exists")
        with open(epoch_id_file) as f:
            epoch_id = int(f.read())
    print("epoch_id ", epoch_id)#; exit()
    

    ## load the best model
    print("loading the best trained model")
    best_model_path = saving_dir + "checkpoint_{}.pt".format(epoch_id)
    best_checkpoint = torch.load(best_model_path )
    model.load_state_dict(best_checkpoint['model'])


    # test the best model
    # accleration
    # net force and acceleration have the same relative loss because all particles have the same mass
    model.eval()
    with torch.no_grad():
        valid_acc_loss, relative_relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
        test_acc_loss, relative_test_acc_loss = cmpt_acc_loss(test_loader, model)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, valid acc: {}, test acc: {}\n".format(exp_id, valid_acc_loss, test_acc_loss))
        f.write("exp_id: {}, relative valid acc: {}, relative test acc: {}\n".format(exp_id, relative_relative_valid_acc_loss, relative_test_acc_loss))
    else:
        print("exp_id: {}, valid acc: {}, test acc: {}\n".format(exp_id, valid_acc_loss, test_acc_loss))
        print("exp_id: {}, relative valid acc: {}, relative test acc: {}\n".format(exp_id, relative_relative_valid_acc_loss, relative_test_acc_loss))

    # pairwise force
    model.eval()
    with torch.no_grad():
        valid_pairwise_force_loss, relative_valid_pairwise_force_loss = cmpt_pairwise_force_loss(valid_loader, model)
        test_pairwise_force_loss, relative_test_pairwise_force_loss = cmpt_pairwise_force_loss(test_loader, model)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, valid pairwise force: {}, test pairwise force: {}\n".format(exp_id, valid_pairwise_force_loss, test_pairwise_force_loss))
        f.write("exp_id: {}, relative valid pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, relative_valid_pairwise_force_loss, relative_test_pairwise_force_loss))
    else:
        print("exp_id: {}, valid pairwise force: {}, test pairwise force: {}\n".format(exp_id, valid_pairwise_force_loss, test_pairwise_force_loss))
        print("exp_id: {}, relative valid pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, relative_valid_pairwise_force_loss, relative_test_pairwise_force_loss))


    # force symmetry
    model.eval()
    with torch.no_grad():
        valid_force_symmetry_loss, relative_valid_force_symmetry_loss = cmpt_force_symmetry(valid_loader, model)
        test_force_symmetry_loss, relative_test_force_symmetry_loss = cmpt_force_symmetry(test_loader, model)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, force symmetry: {}, force symmetry: {}\n".format(exp_id, valid_force_symmetry_loss, test_force_symmetry_loss))
        f.write("exp_id: {}, relative force symmetry: {}, relative force symmetry: {}\n".format(exp_id, relative_valid_force_symmetry_loss, relative_test_force_symmetry_loss))
    else:
        print("exp_id: {}, force symmetry: {}, force symmetry: {}\n".format(exp_id, valid_force_symmetry_loss, test_force_symmetry_loss))
        print("exp_id: {}, relative force symmetry: {}, relative force symmetry: {}\n".format(exp_id, relative_valid_force_symmetry_loss, relative_test_force_symmetry_loss))


    if not os.path.exists(epoch_id_file):
        f.close()
        print("Results saved in ", output_dir + "results_{}.txt".format(exp_id))

    

