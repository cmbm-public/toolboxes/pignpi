import numpy as np
import pickle
import torch
from torch.utils import data
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pad_sequence
from sklearn.preprocessing import StandardScaler


torch.set_default_dtype(torch.float64)


class MyDataset(data.Dataset):
    def __init__(self, edge_rec, edge_send, in_feature, acc, num_edges, edge_distance):
        self.edge_rec = edge_rec # shape: [num_time_steps, num_edge, 1]
        self.edge_send = edge_send # shape: [num_time_steps, num_edge, 1]
        self.num_edges = num_edges # a list containing the number of edges at each time step. Length: (num_time_steps)
        self.in_feature = in_feature # shape: [num_time_steps, num_node, 6]; loc + vel. No mass here
        self.acc = acc # shape: [num_time_steps, num_node, 3]
        self.graph_ids = [i for i in range(len(self.num_edges) ) ]
        self.edge_distance = edge_distance

        Nav = 6.022e23 # Molecules/mol
        self.mass = (39.95/Nav)*(10**-3) # mass of a single atom , Kg
        self.num_nodes = 258
    
    def __len__(self):
        # Number of samples in each epoch
        return len(self.num_edges)
    
    def __getitem__(self, index):
        if self.in_feature == None:
            return self.edge_rec[index], self.edge_send[index], self.num_edges[index], self.graph_ids[index], None, self.acc[index], torch.from_numpy(self.edge_distance[index])
        else:
            return self.edge_rec[index], self.edge_send[index], self.num_edges[index], self.graph_ids[index], self.in_feature[index], self.acc[index], torch.from_numpy(self.edge_distance[index])


def collate_fn(batch):
    edge_rec, edge_send, num_edges, graph_ids, in_feature, acc, edge_distance = zip(*batch)
    ### change tuple to torch.tensor
    if in_feature[0] is not None:
        in_feature = torch.stack(in_feature)#.cuda()
    else:
        in_feature = None
    acc = torch.stack(acc)#.cuda()

    num_graph = len(edge_rec) # the number of graphs in this batch
    max_edge_number = np.max(num_edges)
    sparse_edge_node_mat_shape = (num_graph, max_edge_number, 258)

    edge_mat_indices_dim0 = [] # num_steps
    edge_mat_indices_dim1 = [] # num_edge
    edge_rec_mat_indices_dim2 = [] # num_nodes. The receive nodes in the rec_mat
    edge_send_mat_indices_dim2 = [] # num_nodes. The send nodes in the send_mat

    edge_feature = torch.zeros((num_graph, max_edge_number, 3)) # dx, dy, dz
    for i in range(num_graph): # todo: padding function here?
        edge_feature[i, 0:num_edges[i], :] = edge_distance[i][:, :]

    for i in range(num_graph):
        edge_mat_indices_dim0 += [i] * num_edges[i] # graph_id is i, repeat with num_edges[i] edges
        edge_mat_indices_dim1 += [itr for itr in range(num_edges[i])] # the edge_ids in each graph
        edge_rec_mat_indices_dim2 += edge_rec[i].tolist()
        edge_send_mat_indices_dim2 += edge_send[i].tolist()

    edge_mat_indices_dim0 = torch.LongTensor(edge_mat_indices_dim0) # graph id
    edge_mat_indices_dim1 = torch.LongTensor(edge_mat_indices_dim1) # edge id
    edge_rec_mat_indices_dim2 = torch.LongTensor(edge_rec_mat_indices_dim2) # receive node ids
    edge_send_mat_indices_dim2 = torch.LongTensor(edge_send_mat_indices_dim2)
    edge_mat_values = torch.ones(len(edge_mat_indices_dim0), dtype=torch.float64)


    num_edges_list_tensor = torch.tensor(num_edges)
    
    return in_feature, acc, edge_feature, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, num_edges_list_tensor


def load_data_xyzvxvyvz_dxdydz(input_dir, batch_size, saving_dir):
    print("batch_size: {}".format(batch_size))
    
    Nav = 6.022e23 # Molecules/mol
    mass = (39.95/Nav)*(10**-3) # mass of a single atom , Kg
    num_nodes = 258

    loc = [] # [num_sim*time_steps, num_node, 3]
    vel = [] # [num_sim*time_steps, num_node, 3]
    acc = [] # [num_sim*time_steps, num_node, 3]
    edge_rec = [] # the edges of different time steps of different simulations. Shape:[num_sim*time_steps][num_edges in this time_step]
    edge_send = []
    edge_distance = []


    for exp in range(10):
        for i in range(1000):
            this_xyz = np.load(input_dir + 'step{}_{}_xyz.npy'.format(exp, i) )
            this_vxvyvz = np.load(input_dir + "step{}_{}_vxvyvz.npy".format(exp, i) )
            this_axayaz = np.load(input_dir + "step{}_{}_netForce.npy".format(exp, i) )
            loc.append(this_xyz)
            vel.append(this_vxvyvz)
            acc.append(this_axayaz)

            this_edge = np.load(input_dir + "step{}_{}_edges.npy".format(exp, i))
            edge_rec.append(this_edge[:, 0])
            edge_send.append(this_edge[:, 1])

            this_edge_distance = np.load(input_dir + "step{}_{}_true_distance.npy".format(exp, i)) # shape: [num_edge, 3]
            this_edge_distance = this_edge_distance / 27.27064966799712 # normalize edge distance
            edge_distance.append(this_edge_distance)     

    loc = np.array(loc)
    vel = np.array(vel)
    acc = np.array(acc)
    # assert len(loc) == 10000

    loc_train = loc[0:7000, :, :]
    vel_train = vel[0:7000, :, :]
    acc_train = acc[0:7000, :, :]

    loc_train = loc_train / 27.27064966799712 # normalize the loc
    

    # normalize vel
    numTimeSteps, numNode, dim  = vel_train.shape
    vel_train_flatten = vel_train.reshape((numTimeSteps * numNode, dim))
    assert numNode == num_nodes
    
    input_scalar = StandardScaler()
    input_scalar.fit(vel_train_flatten)
    vel_train_flatten = input_scalar.transform(vel_train_flatten)
    input_normalization = {
        "mean": input_scalar.mean_,
        "var": input_scalar.var_
    }
    print("input_normalization: ", input_normalization)
    vel_train = vel_train_flatten.reshape((numTimeSteps, numNode, dim))
    training_input = np.concatenate([loc_train, vel_train], axis=2)
    

    # unit mass
    masses = np.ones((7000, num_nodes, 1), dtype=training_input.dtype)
    training_input = np.concatenate([training_input, masses], axis=2) # shape: force :[7000, num_nodes, 7] or potential: [7000, num_nodes, 4]
    print("training_input shape: ", training_input.shape)


    acc_train_var = np.var(acc_train)
    training_target = acc_train / np.sqrt(acc_train_var)
    print("training_target shape: ", training_target.shape)
    target_normalization = {"var": acc_train_var}
    print("target_normalization: ", target_normalization)


    with open(saving_dir + 'normalization_info.pkl', 'wb') as f:
        pickle.dump([input_normalization, target_normalization], f)

    train_edge_rec = edge_rec[0:7000]
    train_edge_send = edge_send[0:7000]
    train_edge_distance = edge_distance[0:7000]

    train_node_feat = torch.from_numpy(training_input) # omit mass here
    training_target = torch.from_numpy(training_target)
    edge_num_list = []
    for i in range(7000):
        edge_num_list.append(len(train_edge_rec[i]) )

    
    training_dataset = MyDataset(train_edge_rec, train_edge_send, train_node_feat, training_target, edge_num_list, train_edge_distance)
    training_loader = data.DataLoader(training_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn, num_workers=8, pin_memory=True)

    return training_loader


   