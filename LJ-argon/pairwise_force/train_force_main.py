'''
The main script to train PIGNPI, baseline or GN+ to learn pairwise force
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0" # change the GPU ID
import numpy as np
import torch
torch.set_num_threads(2)

import pickle as pkl

from torch.optim import Adam
from tqdm import tqdm
import math
import argparse


from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR


from GNN_models import PIGNPI_force, Baseline_force #, PIGNPI_force, Baseline_force 
from GNN_models import lemos2021rediscovering
from utils import load_data_xyzvxvyvz_dxdydz 

parser = argparse.ArgumentParser()
parser.add_argument('--no_cuda', action='store_true', default=False, help='Disables CUDA training.')
parser.add_argument('--space_dim', type=int, default=3, help='3 for the LJ-Argon.')
parser.add_argument('--batch_size', type=int, default=16, help='Batch size.')
parser.add_argument('--num_nodes', type=int, default=258, help='Number of nodes.')
parser.add_argument('--input_dim', type=int, default=7, help='node position and node velocity and (unit) mass.')
parser.add_argument('--hidden_dim', type=int, default=128, help='width of hidden layers.')
parser.add_argument('--init_lr', type=float, default=1e-3, help='Initial learning rate.')
parser.add_argument('--model', type=str, default='pignpi', help='Type of decoder model ([pignpi] or [baseline] or [lemos2021rediscovering]).')
parser.add_argument('--epochs', type=int, default=500, help='Number of epochs to train.')
parser.add_argument('--num_train', type=int, default=7000, help='Number of time steps for training.')
parser.add_argument('--task_type', type=str, default="force", help='learn force [force]')
parser.add_argument('--lemos_same_mass', type=str, default='True', help='Whether assign individual scalar to nodes.')
# parser.add_argument('--box_size', type=float, default=-1.0, help='box size (-1.0 or 27.27064966799712)')

args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
assert args.task_type == "force"
assert args.lemos_same_mass == "True"
assert args.input_dim == 7 # pos + vel + mass
print("args")
print(args)

torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss


for exp_id in [1, 2, 3, 4, 5]: # the experiment IDs. We train the model with 5 experiments to compute the average and std.
    for model_flag in [args.model ]: # "pignpi" for PIGNPI; or "baseline" for the baseline model; or "lemos2021rediscovering" for GN+
        if model_flag == "pignpi":           
            saving_dir = "./{}/exp{}_trained_model_{}/PIGNPI_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.epochs)
            print("--------\nEXP {} LJ-Argon, Train PIGNPI, to save in {}".format(exp_id, saving_dir) )
        elif model_flag == "baseline":
            saving_dir = "./{}/exp{}_trained_model_{}/baseline_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", args.epochs)
            print("--------\nEXP {} LJ-Argon, Train baseline model, to save in {}".format(exp_id, saving_dir) )
        elif model_flag == "lemos2021rediscovering":
            if args.lemos_same_mass == "False":
                saving_dir = "./{}/exp{}_trained_model_{}/{}_xyzvxvyvz_dxdydz_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", model_flag, args.epochs)
            else:
                saving_dir = "./{}/exp{}_trained_model_{}/{}_xyzvxvyvz_dxdydz_sameMass_{}epoch/".format(args.task_type, exp_id, "LJ-Argon", model_flag, args.epochs)
            print("--------\nEXP {} LJ-Argon, Train lemos2021rediscovering model, to save in {}".format(exp_id, saving_dir) )
        else:
            print("model_flag error")
            exit()
        # assert not os.path.isdir(saving_dir)
        if os.path.isdir(saving_dir):
            import shutil
            shutil.rmtree(saving_dir)

        if not os.path.isdir(saving_dir):                
            from pathlib import Path
            Path(saving_dir).mkdir(parents=True, exist_ok=False)


    

        ## load simulation data
        simulation_data_dir = "../dataset/GNN_input/"        
        train_loader = load_data_xyzvxvyvz_dxdydz(simulation_data_dir, args.batch_size, saving_dir)
        

        
        # model
        if model_flag == "pignpi":
            model = PIGNPI_force(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = args.space_dim, hidden_dim = args.hidden_dim)
            if args.cuda:
                model = model.cuda()
            else:
                model = model.cpu()
            
        elif model_flag == "baseline":
            model = Baseline_force(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = args.space_dim, hidden_dim = args.hidden_dim)
            if args.cuda:
                model = model.cuda()
            else:
                model = model.cpu()
        
        elif model_flag == "lemos2021rediscovering":
            if args.lemos_same_mass == "False":
                lemos_same_mass_flag = False
            else:
                lemos_same_mass_flag = True
            model = lemos2021rediscovering(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = args.space_dim, hidden_dim = args.hidden_dim, same_mass = lemos_same_mass_flag)
            if args.cuda:
                model = model.cuda()
            else:
                model = model.cpu()
        else:
            print("not valide model")
            exit()


        # training
        opt = torch.optim.Adam(model.parameters(), lr=args.init_lr, weight_decay=1e-8)

        training_example_number = args.num_train
        print('training_example_number: {}'.format(training_example_number))

        batch_per_epoch = math.ceil(training_example_number / args.batch_size )

        sched = OneCycleLR(opt, max_lr=args.init_lr,
                           steps_per_epoch=batch_per_epoch,#len(train_loader),
                           epochs=args.epochs, final_div_factor=1e5)

        print("batch_per_epoch: {}".format(batch_per_epoch))


        def cmpt_loss(input_dataloader, model):
            # compute the acceleration loss 
            total_loss = 0.0; num_items = 0
            for batch_idx, (feat, acc, edge_feature, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, _) in tqdm(enumerate(input_dataloader)):
                if feat is not None:
                    feat = feat.cuda()
                acc = acc.cuda()
                edge_feature = edge_feature.cuda()

                num_graph, max_edge_number, _ = edge_feature.shape
                sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
                rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_rec_mat = edge_rec_mat.cuda()
                rec_indices = None

                send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_send_mat = edge_send_mat.cuda()
                send_indices = None

                predicted_acc = model.cmpt_net_force(feat, edge_feature, edge_rec_mat, edge_send_mat)                
                loss = torch.sum(torch.abs(predicted_acc - acc) )
                total_loss += loss.item()
                num_items += len(acc) * args.num_nodes # all the nodes number in this batch
            return total_loss / num_items



        ############# before training
        print("Before training")
        initial_training_loss = cmpt_loss(train_loader, model)
        print("Initial Training Loss: {}".format(initial_training_loss))

        saving_path = saving_dir + "saved_checkpoint_before_training"
        checkpoint = { 
            'model': model.state_dict(),
            'opt': opt.state_dict(),
            'lr_sched': sched,
            'training_loss': initial_training_loss,
        }
        print("saving untrained model at {}".format(saving_path))
        torch.save(checkpoint, saving_path)



        ### start training

        for epoch in tqdm(range(0, args.epochs)):
            total_loss = 0.0
            num_items = 0
            for batch_idx, (feat_train, acc_train, edge_feature_train, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, _) in enumerate(train_loader):
                if args.cuda:
                    if feat_train is not None:
                        feat_train = feat_train.cuda()
                    acc_train = acc_train.cuda()
                    edge_feature_train = edge_feature_train.cuda()
                    

                num_graph, max_edge_number, _ = edge_feature_train.shape
                sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
                rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_rec_mat = edge_rec_mat.cuda()
                rec_indices = None

                send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_send_mat = edge_send_mat.cuda()
                send_indices = None


                opt.zero_grad()           
                predicted_acc = model.cmpt_net_force(feat_train, edge_feature_train, edge_rec_mat, edge_send_mat)

                loss = torch.sum(torch.abs(acc_train - predicted_acc) )

                loss.backward()
                opt.step()
                sched.step()
                total_loss += loss.item()

                num_items += len(acc_train) * args.num_nodes # the number of nodes in this batch

            training_loss = total_loss / num_items

            print("Epoch: {}, training loss: {}".format(epoch, training_loss))

            # save the model current epoch
            saving_path = saving_dir + "checkpoint_{}.pt".format(epoch)
            checkpoint = { 
                'epoch': epoch,
                'model': model.state_dict(),
                'opt': opt.state_dict(),
                'lr_sched': sched,
                'training_loss': training_loss,
            }
            torch.save(checkpoint, saving_path)


            
            
