'''
The script to test the trained models to learn pairwise potential, including PIGNPI and baseline 
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
import torch
from torch.autograd import Variable
from matplotlib import pyplot as plt
import pickle as pkl
from torch.optim import Adam
from tqdm import tqdm
import math
import argparse

torch.set_num_threads(2)
torch.set_printoptions(precision=10)

from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR

from GNN_models import PIGNPI_potential, Baseline_potential 
from utils_test import load_data_test



parser = argparse.ArgumentParser()
parser.add_argument('--no_cuda', action='store_true', default=False, help='Disables CUDA training.')
parser.add_argument('--space_dim', type=int, default=3, help='3 for the LJ-Argon.')
parser.add_argument('--batch_size', type=int, default=1, help='Batch size.')
parser.add_argument('--num_nodes', type=int, default=258, help='Number of nodes.')
parser.add_argument('--input_dim', type=int, default=7, help='2 * args.space_dim + 1 # node position, node velocity, node mass.')
parser.add_argument('--hidden_dim', type=int, default=128, help='width of hidden layers.')
parser.add_argument('--epochs', type=int, default=500, help='Number of epochs to train. Potential: 200 for pignpi and 500 for baseline.')
parser.add_argument('--num_train', type=int, default=7000, help='Number of time steps for training.')
parser.add_argument('--num_valid', type=int, default=1500, help='Number of simulation steps used for validation.')
parser.add_argument('--num_test', type=int, default=1500, help='Number of simulation steps used for testing.')
parser.add_argument('--task_type', type=str, default="potential", help='[potential]')
parser.add_argument('--model_flag', type=str, default="baseline", help='choose the model [pignpi] or [baseline]')
parser.add_argument('--box_size', type=float, default=27.27064966799712, help='box size')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
assert args.batch_size == 1 # easier to compute the pairwise force and pairwise potential increment
assert args.task_type == "potential"
assert args.input_dim == 7

print(args)

torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss


# the function to compute the acceleration loss
def cmpt_acc_loss(input_dataloader, model):
    """acceleration is the normalized net force here"""
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        feat = feat.cuda()
        acc = acc.cuda()
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        predicted_acc = model.cmpt_net_force(feat, edge_rec_mat, edge_send_mat)                
        loss = torch.sum(torch.abs(predicted_acc - acc) )
        total_loss += loss.item()
        num_items += len(feat) * 258 # all the nodes number in this batch
        ground_truth_magnitude += torch.sum(torch.abs(acc) ).item() # the magnitude of ground-truth acc
    return total_loss / num_items, total_loss / ground_truth_magnitude






def cmpt_pairwise_force_loss(input_dataloader, model):
    """compute the pairwise force loss on validation or testing dataloader"""
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    assert args.batch_size == 1
    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in tqdm(enumerate(input_dataloader)):
        max_len = pairwise_force.shape[1] # max edge numbers in this batch

        pairwise_force = pairwise_force.cuda() # ground-truth pairwise force

        feat = feat.cuda()

        # sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.to_dense()
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.to_dense()
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        
        rec_node_feat = feat.clone().detach().requires_grad_(False)
        send_node_feat = feat.clone().detach().requires_grad_(False)

        sender_input_state = torch.bmm(edge_send_mat, send_node_feat)
        receiver_input_state = torch.bmm(edge_rec_mat, rec_node_feat)

        receiver_input_state.requires_grad_(True) # to compute the partial derivative with the recervers' positions

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / args.box_size)

        edge_feature = diff_pos - args.box_size * diff_pos_rounding

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        m12 = model.msg_fnc(pre_msg)
        m12 = torch.squeeze(m12)

        m12 = m12 * torch.sqrt(output_var) 

        predicted_edge_force = - torch.autograd.grad(m12.sum(), receiver_input_state, retain_graph=True)[0]
        predicted_edge_force = predicted_edge_force[:, :, 0:3] # * torch.sqrt(output_var) # denormalize the output


        error = torch.sum(torch.abs(predicted_edge_force -  pairwise_force ) )
        total_loss += error.item()
        num_items += num_edges_list_tensor.sum().item()
        ground_truth_magnitude += torch.sum(torch.abs(pairwise_force ) ).item()
    print("## In cmpt_pairwise_force_loss, average pairwise force: {}".format(ground_truth_magnitude / num_items ) )
    return total_loss / num_items, total_loss / ground_truth_magnitude


def cmpt_potential_average_difference(input_dataloader, model):
    '''
    compute the average pairwise potential energy
    '''
    assert args.batch_size == 1
    num_edges = 0
    predicted_edge_potential_sum = 0.0
    truth_edge_potential_sum = 0.0

    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in tqdm(enumerate(input_dataloader)):
        max_len = pairwise_force.shape[1] # max edge numbers in this batch
        feat = feat.cuda()

        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.to_dense()
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.to_dense()
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        rec_node_feat = feat.clone().detach().requires_grad_(False)
        send_node_feat = feat.clone().detach().requires_grad_(False)

        sender_input_state = torch.bmm(edge_send_mat, send_node_feat)
        receiver_input_state = torch.bmm(edge_rec_mat, rec_node_feat)

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / args.box_size)

        edge_feature = diff_pos - args.box_size * diff_pos_rounding

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        m12 = model.msg_fnc(pre_msg)
        m12 = m12 / 10 ## PAY ATTENTION. since the position's unit is angstrom, force's unit is unit.kilojoules_per_mole/unit.nanometer
        m12 = m12 * torch.sqrt(output_var)

        m12 = m12.squeeze().detach().cpu().numpy()
        pairwise_potential = pairwise_potential.squeeze().detach().cpu().numpy()

        predicted_edge_potential_sum += m12.sum()
        truth_edge_potential_sum += pairwise_potential.sum()
        assert len(m12) == len(pairwise_potential)
        num_edges += len(m12)

    average_diff = (predicted_edge_potential_sum - truth_edge_potential_sum) / num_edges
    return average_diff



## the function to compute net potential error
## pay attention to the unit
## Also, pay attention the edge number of each node can change at different time steps. So, need to compute the increment at edge level and then aggregate to nodes
def cmpt_net_potential(input_dataloader, model, pairwise_potential_average_diff):
    '''
    pairwise_potential_average_diff = Average(predicted_pairwise_potential - truth_edge_potential)
    '''
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    assert args.batch_size == 1

    predict_potential_every_node_dict = {}
    true_potential_every_node_dict = {}
    for i in range(args.num_nodes):
        predict_potential_every_node_dict[i] = []
        true_potential_every_node_dict[i] = []

    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        max_len = pairwise_potential.shape[1]
        feat = feat.cuda()

        # sparse_edge_node_mat_shape = (num_graph, max_edge_number, args.num_nodes)
        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        sender_input_state = torch.bmm(edge_send_mat, feat)
        receiver_input_state = torch.bmm(edge_rec_mat, feat)

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / args.box_size)
        edge_feature = diff_pos - args.box_size * diff_pos_rounding

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        m12 = model.msg_fnc(pre_msg)
        
        m12 = m12 / 10 ## force unit is KJ/nanometer, position unit is angstrom
        m12 = m12 * torch.sqrt(output_var) ## de-normalization

        m12 = m12 - pairwise_potential_average_diff # substract the free constant in the potential energy
        
        predicted_net_potential = torch.bmm(edge_rec_mat.transpose(-2, -1), m12) # the aggregated potential on every node
        
        # For computing the relative net potential loss
        ground_truth_magnitude += torch.sum(torch.abs(net_potential)).item()

        predicted_net_potential = predicted_net_potential.squeeze().detach().cpu().numpy()
        net_potential = net_potential.squeeze().detach().cpu().numpy()
        assert len(predicted_net_potential) == len(net_potential)
        assert len(predicted_net_potential) == args.num_nodes

        for i in range(args.num_nodes ):            
            predict_potential_every_node_dict[i].append(predicted_net_potential[i])
            true_potential_every_node_dict[i].append(net_potential[i])


    for i in range(args.num_nodes ):
        for j in range(0, len(predict_potential_every_node_dict[i] ) ):
            predict_net_potential_increment = predict_potential_every_node_dict[i][j] - predict_potential_every_node_dict[i][0]
            true_net_potential_increment = true_potential_every_node_dict[i][j] - true_potential_every_node_dict[i][0]
            error = abs(predict_net_potential_increment - true_net_potential_increment)
            total_loss += error
            num_items += 1

    # print("## In cmpt_net_potential, num_items: {}".format(num_items ) )
    print("## In cmpt_net_potential, average net potential: {}".format(ground_truth_magnitude / num_items ) )
    return total_loss / num_items, total_loss / ground_truth_magnitude


# save the potential for every possible edge
### Pay attention to the units
## position: unit.angstrom (10^-10 meter), epsilon: unit.kilocalories_per_mole, force: unit.kilojoules_per_mole/unit.nanometer, 
## nanometer is 10^-9 meter
## potential energy: unit.kilojoules_per_mole.
## ==> grad(potential / 10) / grad(pos) = force 
def cmpt_pairwise_potential(input_dataloader, model):
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    assert args.batch_size == 1

    predict_potential_every_edge_dict = {}
    true_potential_every_edge_dict = {}
    for i in range(args.num_nodes):
        for j in range(args.num_nodes):
            if i == j:
                continue
            else:
                predict_potential_every_edge_dict[i, j] = []
                true_potential_every_edge_dict[i,j] = []

    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in tqdm(enumerate(input_dataloader)):
        max_len = pairwise_force.shape[1] # max edge numbers in this batch

        feat = feat.cuda()

        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.to_dense()
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.to_dense()
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None

        rec_node_feat = feat.clone().detach().requires_grad_(False)
        send_node_feat = feat.clone().detach().requires_grad_(False)

        sender_input_state = torch.bmm(edge_send_mat, send_node_feat)
        receiver_input_state = torch.bmm(edge_rec_mat, rec_node_feat)

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / args.box_size)

        edge_feature = diff_pos - args.box_size * diff_pos_rounding

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        m12 = model.msg_fnc(pre_msg)
        m12 = m12 / 10 ## since the position's unit is angstrom, force's unit is unit.kilojoules_per_mole/unit.nanometer
        m12 = m12 * torch.sqrt(output_var)

        assert pairwise_potential.shape == m12.shape

        ground_truth_magnitude += torch.sum(torch.abs(pairwise_potential)).item()

        m12 = m12.squeeze().detach().cpu().numpy()
        pairwise_potential = pairwise_potential.squeeze().detach().cpu().numpy()
        edge_rec_mat_indices_dim2 = edge_rec_mat_indices_dim2.cpu().detach().numpy()
        edge_send_mat_indices_dim2 = edge_send_mat_indices_dim2.cpu().detach().numpy()
        assert len(edge_rec_mat_indices_dim2) == len(m12)

        for i in range(len(edge_rec_mat_indices_dim2)):
            send_v = edge_send_mat_indices_dim2[i]
            rec_v = edge_rec_mat_indices_dim2[i]
            predict_potential_every_edge_dict[send_v, rec_v].append(m12[i])
            true_potential_every_edge_dict[send_v, rec_v].append(pairwise_potential[i])

    
    for send_v in range(args.num_nodes):
        for rec_v in range(args.num_nodes):
            if rec_v == send_v:
                continue
            this_predicted_pairwise_potential_list = predict_potential_every_edge_dict[send_v, rec_v]
            this_true_pairwise_potential_list = true_potential_every_edge_dict[send_v, rec_v]
            for j in range(0, len(this_predicted_pairwise_potential_list)):
                predict_pairwise_potential_increment = this_predicted_pairwise_potential_list[j] - this_predicted_pairwise_potential_list[0]
                true_pairwise_potential_increment = this_true_pairwise_potential_list[j] - this_true_pairwise_potential_list[0]
                error = abs(predict_pairwise_potential_increment - true_pairwise_potential_increment)
                total_loss += error
                num_items += 1

    print("## In cmpt_pairwise_potential, num_items: {}, average pairwise potential: {}".format(num_items, ground_truth_magnitude/num_items))
    return total_loss / num_items, total_loss / ground_truth_magnitude




## the function to compute the symmetry error
def cmpt_potential_symmetry(input_dataloader, model):
    total_loss = 0.0; num_items = 0; ground_truth_magnitude = 0.0
    for batch_idx, (feat, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges_list_tensor, pairwise_force, pairwise_potential, net_force, net_potential) in enumerate(input_dataloader):
        max_len = pairwise_potential.shape[1]
        feat = feat.cuda()

        rec_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2))
        edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_rec_mat = edge_rec_mat.cuda()
        rec_indices = None

        send_indices = torch.vstack((edge_mat_indices_dim0, edge_mat_indices_dim1, edge_send_mat_indices_dim2))
        edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
        edge_send_mat = edge_send_mat.cuda()
        send_indices = None


        node_pos = feat[:, :, 0:3]
        node_mass = feat[:, :, 3:7] #  velocity and mass


        rec_node_feat = feat.clone().detach().requires_grad_(False)
        send_node_feat = feat.clone().detach().requires_grad_(False)

        sender_input_state = torch.bmm(edge_send_mat, send_node_feat)
        receiver_input_state = torch.bmm(edge_rec_mat, rec_node_feat)

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / args.box_size)

        edge_feature = diff_pos - args.box_size * diff_pos_rounding


        pre_msg12 = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        pre_msg21 = torch.cat([receiver_input_state, sender_input_state, - edge_feature], dim=-1) # negative edge_feature
        m12 = model.msg_fnc(pre_msg12)
        m21 = model.msg_fnc(pre_msg21)
        # make sure the prediction and the ground-truth are with same unit
        m12 = m12 / 10
        m12 = m12 * torch.sqrt(output_var)
        m21 = m21 / 10
        m21 = m21 * torch.sqrt(output_var)

        mask = torch.arange(max_len).expand(len(num_edges_list_tensor), max_len) < num_edges_list_tensor.unsqueeze(1)
        mask = mask.int().float()
        mask = mask[:, :, None] # expand the dimension, for broadcase
        mask = mask.cuda()

        m12 = m12 * mask
        m21 = m21 * mask

        error = torch.sum(torch.abs(m12 - m21 ) ) # potential should be same on two directed edges
        total_loss += error.item()
        num_items += num_edges_list_tensor.sum().item()

        pairwise_potential = pairwise_potential.cuda()
        pairwise_potential = pairwise_potential * mask
        ground_truth_magnitude += torch.sum(torch.abs(pairwise_potential)).item()
    print("## In cmpt_potential_symmetry, average pairwise potential: {}".format(ground_truth_magnitude / num_items ) )
    return total_loss / num_items, total_loss / ground_truth_magnitude



for exp_id in [1, 2, 3, 4, 5]:
    if args.model_flag == "pignpi":
        assert args.task_type == "potential"
        saving_dir = "./{}/exp{}_trained_model_{}/PIGNPI_xyzvxvyvz_dxdydz_{}epoch/".format("potential", exp_id, "LJ-Argon", args.epochs)
        print("test the trained pignpi potential, saved in: ", saving_dir)
    elif args.model_flag == "baseline":
        assert args.task_type == "potential"
        saving_dir = "./{}/exp{}_trained_model_{}/baseline_xyzvxvyvz_dxdydz_{}epoch/".format("potential", exp_id, "LJ-Argon", args.epochs)
        print("test the trained baseline potential, saved in: ", saving_dir )
    else:
        print("args.model_flag error")
        exit()

    # trained model should exist
    assert os.path.isdir(saving_dir)


    output_dir = "./evaluate_{}_model/{}/{}_{}epoch/".format("potential", args.task_type, args.model_flag, args.epochs)
    if not os.path.isdir(output_dir):                
        from pathlib import Path
        Path(output_dir).mkdir(parents=True, exist_ok=False)


    with open(saving_dir + "normalization_info.pkl", "rb") as f:
        [input_normalization, target_normalization] = pkl.load(f)
    output_var = target_normalization["var"]
    print("output_var: ", output_var)
    output_var = torch.tensor(output_var).cuda()
    print("input normalization")
    print(input_normalization)
    print("target normalization")
    print(target_normalization)
    


    ## load simulation data
    simulation_data_dir = "../dataset/GNN_input/"

    print("loading validation data")
    valid_loader = load_data_test(simulation_data_dir, args.batch_size, saving_dir, "valid", args.task_type)
    print("loading testing data")
    test_loader = load_data_test(simulation_data_dir, args.batch_size, saving_dir, "test", args.task_type)

    
    # potential model
    if args.model_flag == "pignpi":        
        model = PIGNPI_potential(n_node = args.num_nodes, input_dim=args.input_dim, msg_dim=1, hidden_dim=args.hidden_dim)
        if args.cuda:
            model = model.cuda()
        else:
            model = model.cpu()
        
    elif args.model_flag == "baseline":
        model = Baseline_potential(n_node = args.num_nodes, input_dim=args.input_dim, msg_dim=1, hidden_dim=args.hidden_dim)
        if args.cuda:
            model = model.cuda()
        else:
            model = model.cpu()        
    else:
        print("not valide model")
        exit()

    

    ########################################################
    ########################################################
    ########################################################
    ## training loss
    training_loss_list = []
    checkpoint_path = saving_dir + "saved_checkpoint_before_training"
    checkpoint = torch.load(checkpoint_path)
    training_loss_list.append(checkpoint['training_loss'])
    for epoch in tqdm(range(0, args.epochs)):
        checkpoint_path = saving_dir + "checkpoint_{}.pt".format(epoch)
        checkpoint = torch.load(checkpoint_path)
        training_loss_list.append(checkpoint['training_loss'])
        
    training_loss_list = np.array(training_loss_list)

    print("expid: {}, min_training_loss: {}".format(exp_id, np.min(training_loss_list)))
    
    np.savetxt(output_dir + "training_loss_{}.txt".format(exp_id), training_loss_list)


    epoch_id_file = output_dir + "exp{}_epoch_id.txt".format(exp_id)

    if not os.path.exists(epoch_id_file):
        print("load the best model from {} epochs".format(args.epochs))
        ########################################################
        ########################################################
        ########################################################
        ### extract and test the best model
        min_valid_loss = 10000
        relative_valid_acc_loss = -1
        epoch_id = -1 # the epoch_id of best trained model
        checkpoint_path = saving_dir + "saved_checkpoint_before_training"
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint['model'])

        valid_acc_loss, relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
        min_valid_loss = valid_acc_loss

        for epoch in tqdm(range(args.epochs-100, args.epochs)):
            checkpoint_path = saving_dir + "checkpoint_{}.pt".format(epoch)
            checkpoint = torch.load(checkpoint_path)
            model.load_state_dict(checkpoint['model'])
            
            valid_acc_loss, relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
            if min_valid_loss > valid_acc_loss:
                min_valid_loss = valid_acc_loss
                epoch_id = epoch

        print("Exp {}, best trained model is at epoch {}, with valid acc loss {}, relative_valid_acc_loss {}".format(exp_id, epoch_id, min_valid_loss, relative_valid_acc_loss) )

        f = open(output_dir + "results_{}.txt".format(exp_id), "w")
        f.write("Exp {}, best trained model is at epoch {}, with valid acc loss {}, relative_valid_acc_loss {}\n".format(exp_id, epoch_id, min_valid_loss, relative_valid_acc_loss))
    else:
        print("epoch_id info exists")
        with open(epoch_id_file) as f:
            epoch_id = int(f.read())
    print("epoch_id ", epoch_id)#; exit()
    

    ## load the best model
    best_model_path = saving_dir + "checkpoint_{}.pt".format(epoch_id)
    best_checkpoint = torch.load(best_model_path )
    model.load_state_dict(best_checkpoint['model'])
    print("loaded best model...")




    # accleration
    model.eval()
    # with torch.no_grad():
    # valid_acc_loss, relative_relative_valid_acc_loss = cmpt_acc_loss(valid_loader, model)
    test_acc_loss, relative_test_acc_loss = cmpt_acc_loss(test_loader, model)
    if not os.path.exists(epoch_id_file):
        # f.write("exp_id: {}, valid acc: {}, test acc: {}\n".format(exp_id, valid_acc_loss, test_acc_loss))
        # f.write("exp_id: {}, relative valid acc: {}, relative test acc: {}\n".format(exp_id, relative_relative_valid_acc_loss, relative_test_acc_loss))
        f.write("exp_id: {}, test acc: {}, relative test acc: {}\n".format(exp_id, test_acc_loss, relative_test_acc_loss))
    else:
        # print("exp_id: {}, valid acc: {}, test acc: {}\n".format(exp_id, valid_acc_loss, test_acc_loss))
        # print("exp_id: {}, relative valid acc: {}, relative test acc: {}\n".format(exp_id, relative_relative_valid_acc_loss, relative_test_acc_loss))
        print("exp_id: {}, test acc: {}, relative test acc: {}\n".format(exp_id, test_acc_loss, relative_test_acc_loss))



    # pairwise force
    model.eval()
    # valid_pairwise_force_loss, relative_valid_pairwise_force_loss = cmpt_pairwise_force_loss(valid_loader, model)
    test_pairwise_force_loss, relative_test_pairwise_force_loss = cmpt_pairwise_force_loss(test_loader, model)
    if not os.path.exists(epoch_id_file):
        # f.write("exp_id: {}, valid pairwise force: {}, test pairwise force: {}\n".format(exp_id, valid_pairwise_force_loss, test_pairwise_force_loss))
        # f.write("exp_id: {}, relative valid pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, relative_valid_pairwise_force_loss, relative_test_pairwise_force_loss))
        f.write("exp_id: {}, test pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, test_pairwise_force_loss, relative_test_pairwise_force_loss))
    else:
        # print("exp_id: {}, valid pairwise force: {}, test pairwise force: {}\n".format(exp_id, valid_pairwise_force_loss, test_pairwise_force_loss))
        # print("exp_id: {}, relative valid pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, relative_valid_pairwise_force_loss, relative_test_pairwise_force_loss))
        print("exp_id: {}, test pairwise force: {}, relative test pairwise force: {}\n".format(exp_id, test_pairwise_force_loss, relative_test_pairwise_force_loss))


    ### the average of pairwised predicted potential and ground-truth potential 
    ### substract this value from the prediction, when evaluating potentials
    average_diff = cmpt_potential_average_difference(test_loader, model)
    print("average difference of pairwise potential")
    print(average_diff)


    # net potential
    model.eval()
    test_net_potential, relative_test_net_potential = cmpt_net_potential(test_loader, model, average_diff)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, test_net_potential: {}, relative_test_net_potential: {}\n".format(exp_id, test_net_potential, relative_test_net_potential))
    else:
        print("exp_id: {}, test_net_potential: {}, relative_test_net_potential: {}".format(exp_id, test_net_potential, relative_test_net_potential))


    # # pairwise potential
    model.eval()
    test_pairwise_potential, relative_test_pairwise_potential = cmpt_pairwise_potential(test_loader, model)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, test_pairwise_potential: {}, relative_test_pairwise_potential: {}\n".format(exp_id, test_pairwise_potential, relative_test_pairwise_potential))
    else:
        print("exp_id: {}, test_pairwise_potential: {}, relative_test_pairwise_potential: {}".format(exp_id, test_pairwise_potential, relative_test_pairwise_potential))


    # potential symmetry
    model.eval()
    # valid_force_symmetry_loss, relative_valid_force_symmetry_loss = cmpt_potential_symmetry(valid_loader, model)
    test_potential_symmetry_loss, relative_test_potential_symmetry_loss = cmpt_potential_symmetry(test_loader, model)
    if not os.path.exists(epoch_id_file):
        f.write("exp_id: {}, test potential symmetry: {}, relative test potential symmetry: {}\n".format(exp_id, test_potential_symmetry_loss, relative_test_potential_symmetry_loss))
    else:
        print("exp_id: {}, test potential symmetry: {}, relative test potential symmetry: {}\n".format(exp_id, test_potential_symmetry_loss, relative_test_potential_symmetry_loss))


    if not os.path.exists(epoch_id_file):
        f.close()
        print("Results saved in ", output_dir + "results_{}.txt".format(exp_id))
    





