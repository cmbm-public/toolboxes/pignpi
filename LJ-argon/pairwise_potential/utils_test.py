import numpy as np
import pickle
import torch
from torch.utils import data
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader
from torch.nn.utils.rnn import pad_sequence
from sklearn.preprocessing import StandardScaler


torch.set_default_dtype(torch.float64)


class MyDataset_test(data.Dataset):
    def __init__(self, edge_rec, edge_send, in_feature, acc, num_edges, pairwise_force, pairwise_potential, net_force, net_potential ):
        self.edge_rec = edge_rec # shape: [num_time_steps, num_edge (can be different), 1]
        self.edge_send = edge_send # shape: [num_time_steps, num_edge (can be different), 1]
        self.num_edges = num_edges # a list containing the number of edges at each time step. Length: (num_time_steps)
        self.in_feature = in_feature # shape: [num_time_steps, num_node, 6]
        self.acc = acc # shape: [num_time_steps, num_node, 3]

        self.pairwise_force = pairwise_force # shape: [num_time_steps, num_edge (can be different), 3]
        self.pairwise_potential = pairwise_potential # shape: [num_time_steps, num_edge (can be different), 1]
        self.net_force = net_force # shape: [num_time_steps, num_node, 3]
        self.net_potential = net_potential # shape: [num_time_steps, num_node, 1]

        self.graph_ids = [i for i in range(len(self.num_edges) ) ]

        Nav = 6.022e23 # Molecules/mol
        self.mass = (39.95/Nav)*(10**-3) # mass of a single atom , Kg
        self.num_nodes = 258
    
    def __len__(self):
        # Number of samples in each epoch
        return len(self.num_edges)
    
    def __getitem__(self, index):
        return self.edge_rec[index], self.edge_send[index], self.num_edges[index], self.graph_ids[index], self.in_feature[index], self.acc[index], self.pairwise_force[index], self.pairwise_potential[index], self.net_force[index], self.net_potential[index]


def collate_fn_test(batch):
    edge_rec, edge_send, num_edges, graph_ids, in_feature, acc, pairwise_force, pairwise_potential, net_force, net_potential = zip(*batch)
    ### change tuple to torch.tensor
    in_feature = torch.stack(in_feature)
    acc = torch.stack(acc)
    
    ### construct the edge_rec and edge_send
    num_graph = len(edge_rec) # the number of graphs in this batch
    max_edge_number = np.max(num_edges)
    sparse_edge_node_mat_shape = (num_graph, max_edge_number, 258)

    edge_mat_indices_dim0 = [] # num_steps
    edge_mat_indices_dim1 = [] # num_edge
    edge_rec_mat_indices_dim2 = [] # num_nodes. The receive nodes in the rec_mat
    edge_send_mat_indices_dim2 = [] # num_nodes. The send nodes in the send_mat

    for i in range(num_graph):
        edge_mat_indices_dim0 += [i] * num_edges[i] # graph_id is i, repeat with num_edges[i] edges
        edge_mat_indices_dim1 += [itr for itr in range(num_edges[i])] # the edge_ids in each graph
        edge_rec_mat_indices_dim2 += edge_rec[i].tolist()
        edge_send_mat_indices_dim2 += edge_send[i].tolist()

    edge_mat_indices_dim0 = torch.LongTensor(edge_mat_indices_dim0) # graph id
    edge_mat_indices_dim1 = torch.LongTensor(edge_mat_indices_dim1) # edge id
    edge_rec_mat_indices_dim2 = torch.LongTensor(edge_rec_mat_indices_dim2) # receive node ids
    edge_send_mat_indices_dim2 = torch.LongTensor(edge_send_mat_indices_dim2)
    edge_mat_values = torch.ones(len(edge_mat_indices_dim0), dtype=torch.float64)


    ### the ground-truth: pairwise_force, pairwise_potential, net_force, net_potential
    pairwise_force_torch = torch.zeros(num_graph, max_edge_number, 3)
    pairwise_potential_torch = torch.zeros(num_graph, max_edge_number, 1)
    
    for i in range(num_graph):
        num_edge_this_graph = num_edges[i]
        pairwise_force_torch[i, 0:num_edge_this_graph] = torch.from_numpy(pairwise_force[i])
        pairwise_potential_torch[i, 0:num_edge_this_graph] = torch.from_numpy(pairwise_potential[i])
    
    net_force_torch = torch.stack(net_force)
    net_potential_torch = torch.stack(net_potential)

    num_edges = torch.tensor(num_edges)

    return in_feature, acc, edge_mat_indices_dim0, edge_mat_indices_dim1, edge_rec_mat_indices_dim2, edge_send_mat_indices_dim2, edge_mat_values, sparse_edge_node_mat_shape, num_edges, pairwise_force_torch, pairwise_potential_torch, net_force_torch, net_potential_torch



def load_data_test(input_dir, batch_size, saving_dir, mode, task):
    '''
    load input feature and the groundtruth
    task: potential
    '''
    assert task == "potential"
    print("batch_size: {}".format(batch_size))


    Nav = 6.022e23 # Molecules/mol
    mass = (39.95/Nav)*(10**-3) # mass of a single atom , Kg
    num_nodes = 258

    loc = [] # [num_sim, time_steps, num_node, 3]
    vel = [] # [num_sim, time_steps, num_node, 3]
    acc = [] # [num_sim, time_steps, num_node, 3]
    edge_rec = [] # the edges of different time steps of different simulations. Shape:[num_sim][time_steps][num_edges in this time_step]
    edge_send = []

    pairwise_force = []
    pairwise_potential = []
    net_force = []
    net_potential = []

    for exp in range(10):
        for i in range(1000):
            this_xyz = np.load(input_dir + 'step{}_{}_xyz.npy'.format(exp, i) )
            this_vxvyvz = np.load(input_dir + "step{}_{}_vxvyvz.npy".format(exp, i) )
            this_axayaz = np.load(input_dir + "step{}_{}_netForce.npy".format(exp, i) ) # all nodes have the same mass
            loc.append(this_xyz)
            vel.append(this_vxvyvz)
            acc.append(this_axayaz)

            this_edge = np.load(input_dir + "step{}_{}_edges.npy".format(exp, i))
            edge_rec.append(this_edge[:, 0])
            edge_send.append(this_edge[:, 1])
            
            # ground-truth should not be normalized
            this_pairwise_force = np.load(input_dir + "step{}_{}_pairwise_force.npy".format(exp, i) ) # shape: [num_edges (can be different), 3]
            this_pairwise_potential = np.load(input_dir + "step{}_{}_pairwise_potential.npy".format(exp, i) ) # shape: [num_edges (can be different), 1]
            this_net_force = np.load(input_dir + "step{}_{}_netForce.npy".format(exp, i) ) # shape: [258, 3] 
            this_net_potential = np.load(input_dir + "step{}_{}_netPotential.npy".format(exp, i) ) # shape: [258, 1]
            pairwise_force.append(this_pairwise_force)
            pairwise_potential.append(this_pairwise_potential)
            net_force.append(this_net_force)
            net_potential.append(this_net_potential)

    loc = np.array(loc)
    vel = np.array(vel)
    acc = np.array(acc)
    assert len(loc) == 10000

    if mode == "valid":
        loc_testing = loc[7000:8500, :, :]
        vel_testing = vel[7000:8500, :, :]
        acc_testing = acc[7000:8500, :, :]
        edge_rec_testing = edge_rec[7000:8500]
        edge_send_testing = edge_send[7000:8500]
        pairwise_force_testing = pairwise_force[7000:8500]
        pairwise_potential_testing = pairwise_potential[7000:8500]
        net_force_testing = net_force[7000:8500]
        net_potential_testing = net_potential[7000:8500]
    elif mode == "test":
        loc_testing = loc[8500:10000, :, :]
        vel_testing = vel[8500:10000, :, :]
        acc_testing = acc[8500:10000, :, :]
        edge_rec_testing = edge_rec[8500:10000]
        edge_send_testing = edge_send[8500:10000]
        pairwise_force_testing = pairwise_force[8500:10000]
        pairwise_potential_testing = pairwise_potential[8500:10000]
        net_force_testing = net_force[8500:10000]
        net_potential_testing = net_potential[8500:10000]


    loc_testing = loc_testing
    
    with open(saving_dir + "./normalization_info.pkl", "rb") as f:
        [input_normalization, target_normalization] = pickle.load(f)
    
    
    input_mean = input_normalization["mean"]
    input_var = input_normalization["var"] ## this is variance, not standard derivation
    vel_testing = (vel_testing - input_mean) / np.sqrt(input_var)
    masses = np.ones((loc_testing.shape[0], num_nodes, 1), dtype = loc_testing.dtype )
    testing_input = np.concatenate([loc_testing, vel_testing, masses], axis=2)


    target_var = target_normalization["var"]
    acc_testing = acc_testing / np.sqrt(target_var)

    
    print("## In utils_test, loading the data info (saved when training)")
    print("input_normalization: {}".format(input_normalization) )
    print("target_normalization: {}".format(target_normalization) )


    print("## In utils, after normalization")
    print("input mean: {}".format(np.mean(testing_input, axis=(0, 1) ) ) )
    print("input variance: {}".format(np.var(testing_input, axis=(0, 1) ) ) )
    print("acc mean: {}".format(np.mean(acc_testing, axis=(0, 1) ) ) )
    print("acc variance: {}".format(np.var(acc_testing, axis=(0, 1) ) ) )


    edge_num_list = []
    for i in range(len(edge_rec_testing)): # i is the exp_id        
        assert len(edge_rec_testing[i]) == len(edge_send_testing[i])
        edge_num_list.append(len(edge_rec_testing[i]))

        assert len(edge_rec_testing[i]) == len(pairwise_force_testing[i])
        assert len(edge_rec_testing[i]) == len(pairwise_potential_testing[i])
        assert len(net_force_testing[i]) == num_nodes
        assert len(net_potential_testing[i]) == num_nodes

    testing_node_feat = torch.from_numpy(testing_input)
    testing_target = torch.from_numpy(acc_testing) 

    net_force_testing = np.array(net_force_testing)
    net_potential_testing = np.array(net_potential_testing)
    net_force_testing = torch.from_numpy(net_force_testing)
    net_potential_testing = torch.from_numpy(net_potential_testing)

    edge_num_list = np.array(edge_num_list)

    ### construct the dataset
    dset = MyDataset_test(edge_rec_testing, edge_send_testing, testing_node_feat, testing_target, edge_num_list, pairwise_force_testing, pairwise_potential_testing, net_force_testing, net_potential_testing)
    dloader = data.DataLoader(dset, batch_size=batch_size, shuffle=False, collate_fn=collate_fn_test, num_workers=4, pin_memory=True)

    return dloader


    


    



