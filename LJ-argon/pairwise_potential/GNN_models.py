'''
The implementation of PIGNPI and the baseline
'''

import numpy as np
import torch
import torch.nn as nn
from torch.functional import F
from torch.optim import Adam
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, GELU, Tanh, Softplus, Sigmoid, LeakyReLU
from torch.autograd import grad

torch.set_default_dtype(torch.float64)



class SiLU(nn.Module):    
    # from https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#SiLU, since it is missing in 1.6.0
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str





class PIGNPI_potential(nn.Module):
    def __init__(self, n_node, input_dim, msg_dim, hidden_dim):
        super(PIGNPI_potential, self).__init__()

        self.num_node = n_node
        self.box_size = 27.27064966799712

        # assert input_dim == 4 #  4: pos, masses
        print("## In PIGNPI_potential, input_dim: ", input_dim)
        assert msg_dim == 1 # the potential is a scalar

        input_dim = 2 * input_dim + 3 # x, y, z, vx, vy, vz, mass, dx, dy, dz

        # self.msg_fnc return the potential energy
        self.msg_fnc = Seq(
            Lin(input_dim, hidden_dim), # pos, vel; for two nodes
            SiLU(),

            Lin(hidden_dim, hidden_dim),
            SiLU(),

            Lin(hidden_dim, hidden_dim),
            SiLU(),

            #(Can turn on or off this layer:)
            Lin(hidden_dim, hidden_dim), 
            SiLU(),
            
            Lin(hidden_dim, 1) # output dimension is 1 because potential energy is a scalar
        )
        self.msg_fnc.apply(self.init_weights)

    def init_weights(self, m):
        if isinstance(m, nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


    def cmpt_net_force(self, inputs, rel_rec, rel_send):

        node_pos = inputs[:, :, 0:3]
        node_mass = inputs[:, :, 3:7] #  velocity and mass
        
        # compute the (dx, dy, dz) as edge feature
        diff_pos = torch.bmm(rel_send, node_pos) - torch.bmm(rel_rec, node_pos) # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / self.box_size)

        rec_node_pos = node_pos.clone().detach().requires_grad_(True)
        node_pos.requires_grad_(False)

        edge_feature = torch.bmm(rel_send, node_pos) - torch.bmm(rel_rec, rec_node_pos)
        edge_feature -= self.box_size * diff_pos_rounding

        rec_node_feat = torch.cat([rec_node_pos, node_mass], dim=-1) 
        send_node_feat = torch.cat([node_pos, node_mass], dim=-1)

        time_steps, num_atoms, num_dims = inputs.shape
        num_timesteps, num_edges, num_particles = rel_rec.shape

        receiver_input_state = torch.bmm(rel_rec, rec_node_feat) # receivers need gradients
        sender_input_state = torch.bmm(rel_send, send_node_feat)
        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)

        edge_msg = self.msg_fnc(pre_msg)#; print(edge_msg.shape); exit()

        force = - torch.autograd.grad(edge_msg.sum(), rec_node_pos, create_graph=True)[0]
        force = force[:, :, 0:3] # first 3 dimensions are the position

        return force





class Baseline_potential(nn.Module):
    def __init__(self, n_node, input_dim, msg_dim, hidden_dim):
        super(Baseline_potential, self).__init__()

        self.num_node = n_node
        assert input_dim == 7 #  4: pos, velocity, masses
        assert msg_dim == 1 # the potential is a scalar
        self.box_size = 27.27064966799712

        self.msg_fnc = Seq(
            Lin(2 * input_dim + 3, hidden_dim), # pos, vel, charge, mass; for two nodes
            ReLU(), # baseline uses ReLU as default
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            #(Can turn on or off this layer:)
            Lin(hidden_dim, hidden_dim), 
            ReLU(),
            Lin(hidden_dim, msg_dim)
        )

        self.node_fnc = Seq(
            Lin(msg_dim+input_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, hidden_dim),
            ReLU(),
            Lin(hidden_dim, 3) # the output dimension is 3
        )
        self.msg_fnc.apply(self.init_weights)
        self.node_fnc.apply(self.init_weights)

    def init_weights(self, m):
        if isinstance(m, nn.Linear):
            print("init Linear !!!!")
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)


    def cmpt_net_force(self, inputs, rel_rec, rel_send):

        time_steps, num_atoms, num_dims = inputs.shape

        receiver_input_state = torch.bmm(rel_rec, inputs) 
        sender_input_state = torch.bmm(rel_send, inputs)

        diff_pos = sender_input_state[:, :, 0:3] - receiver_input_state[:, :, 0:3] # sending - receiving
        diff_pos_rounding = torch.round(diff_pos / self.box_size)
        edge_feature = diff_pos - self.box_size * diff_pos_rounding

        pre_msg = torch.cat([sender_input_state, receiver_input_state, edge_feature], dim=-1)
        edge_msg = self.msg_fnc(pre_msg)#; print(edge_msg.shape); exit()
        # edge_msg shape: [time_steps, all_edges, 1]

        rec_edge_mat = rel_rec.transpose(-2, -1)
        agg_msgs = torch.bmm(rec_edge_mat, edge_msg)

        combined_node_edge = torch.cat([inputs, agg_msgs], dim = -1) # combine node feature and in-coming edge features
        prediction = self.node_fnc(combined_node_edge)
        return prediction

   


