'''
The script to train PIGNPI and baseline to learn pairwise potential energy
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import numpy as np
import torch
torch.set_num_threads(2)

import pickle as pkl

from torch.optim import Adam
from tqdm import tqdm
import math
import argparse


from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR


from GNN_models import PIGNPI_potential, Baseline_potential 
from utils import load_data_xyz_vxvyvz 




parser = argparse.ArgumentParser()
parser.add_argument('--no_cuda', action='store_true', default=False, help='Disables CUDA training.')
parser.add_argument('--space_dim', type=int, default=3, help='3 for the LJ-Argon.')
parser.add_argument('--batch_size', type=int, default=16, help='Batch size.')
parser.add_argument('--num_nodes', type=int, default=258, help='Number of nodes.')
parser.add_argument('--input_dim', type=int, default=7, help='node position and node velocity and (unit) mass.')
parser.add_argument('--hidden_dim', type=int, default=128, help='width of hidden layers.')
parser.add_argument('--init_lr', type=float, default=1e-3, help='Initial learning rate.')
parser.add_argument('--model', type=str, default='pignpi', help='Type of decoder model ([pignpi] or [baseline]).')
parser.add_argument('--epochs', type=int, default=500, help='Number of epochs to train.')
parser.add_argument('--num_train', type=int, default=7000, help='Number of time steps for training.')
parser.add_argument('--task_type', type=str, default="potential", help='[potential]')
# parser.add_argument('--box_size', type=float, default=-1.0, help='box size (-1.0 or 27.27064966799712)')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
assert args.task_type == "potential"
assert args.input_dim == 7

print("args")
print(args)

torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss



for exp_id in [1, 2, 3, 4, 5]:
    for model_flag in [args.model ]: # "pignpi" for PIGNPI; or "baseline" for the baseline model.
        if model_flag == "pignpi":           
            assert args.task_type == "potential"
            saving_dir = "./{}/exp{}_trained_model_{}/PIGNPI_xyzvxvyvz_dxdydz_{}epoch/".format("potential", exp_id, "LJ-Argon", args.epochs)
            print("--------\nEXP {} LJ-Argon, Train PIGNPI, to save in {}".format(exp_id, saving_dir) )
        elif model_flag == "baseline":
            assert args.task_type == "potential"
            saving_dir = "./{}/exp{}_trained_model_{}/baseline_xyzvxvyvz_dxdydz_{}epoch/".format("potential", exp_id, "LJ-Argon", args.epochs)
            print("--------\nEXP {} LJ-Argon, Train baseline model, to save in {}".format(exp_id, saving_dir) )
        else:
            print("model_flag error")
            exit()
        # assert not os.path.isdir(saving_dir)
        if os.path.isdir(saving_dir):
            import shutil
            shutil.rmtree(saving_dir)

        if not os.path.isdir(saving_dir):                
            from pathlib import Path
            Path(saving_dir).mkdir(parents=True, exist_ok=False)


        ## load simulation data
        simulation_data_dir = "../dataset/GNN_input/"        
        train_loader = load_data_xyz_vxvyvz(simulation_data_dir, args.batch_size, saving_dir, args.task_type)
        
        
        # potential models        
        if model_flag == "pignpi":
            model = PIGNPI_potential(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = 1, hidden_dim = args.hidden_dim)
            if args.cuda:
                model = model.cuda()
            else:
                model = model.cpu()
            
        elif model_flag == "baseline":
            model = Baseline_potential(n_node = args.num_nodes, input_dim = args.input_dim, msg_dim = 1, hidden_dim = args.hidden_dim)
            if args.cuda:
                model = model.cuda()
            else:
                model = model.cpu()

        else:
            print("not valide model")
            exit()


        # training
        opt = torch.optim.Adam(model.parameters(), lr=args.init_lr, weight_decay=1e-8)

        training_example_number = args.num_train
        print('training_example_number: {}'.format(training_example_number))

        batch_per_epoch = math.ceil(training_example_number / args.batch_size )

        sched = OneCycleLR(opt, max_lr=args.init_lr,
                           steps_per_epoch=batch_per_epoch,#len(train_loader),
                           epochs=args.epochs, final_div_factor=1e5)

        print("batch_per_epoch: {}".format(batch_per_epoch))


        def cmpt_loss(input_dataloader, model):
            # compute the acceleration loss
            total_loss = 0.0; num_items = 0
            for batch_idx, (feat, acc, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, sparse_edge_node_mat_shape) in tqdm(enumerate(input_dataloader)):
                if feat is not None:
                    feat = feat.cuda()
                acc = acc.cuda()

                rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_rec_mat = edge_rec_mat.cuda()
                rec_indices = None

                send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_send_mat = edge_send_mat.cuda()
                send_indices = None

                predicted_acc = model.cmpt_net_force(feat, edge_rec_mat, edge_send_mat)                
                loss = torch.sum(torch.abs(predicted_acc - acc) )
                total_loss += loss.item()
                num_items += len(acc) * args.num_nodes # all the nodes number in this batch
            return total_loss / num_items



        ############# before training #############
        print("Before training")
        initial_training_loss = cmpt_loss(train_loader, model)
        print("Initial Training Loss: {}".format(initial_training_loss))

        saving_path = saving_dir + "saved_checkpoint_before_training"
        checkpoint = { 
            'model': model.state_dict(),
            'opt': opt.state_dict(),
            'lr_sched': sched,
            'training_loss': initial_training_loss,
        }
        print("saving untrained model at {}".format(saving_path))
        torch.save(checkpoint, saving_path)


        # ### start training
        for epoch in tqdm(range(0, args.epochs)):
            total_loss = 0.0
            num_items = 0
            for batch_idx, (feat_train, acc_train, edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx, edge_node_mat_send_node_idx, edge_mat_values, sparse_edge_node_mat_shape) in enumerate(train_loader):
                if args.cuda:
                    if feat_train is not None:
                        feat_train = feat_train.cuda()
                    acc_train = acc_train.cuda()                    

                rec_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_rec_node_idx))
                edge_rec_mat = torch.sparse.DoubleTensor(torch.LongTensor(rec_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_rec_mat = edge_rec_mat.cuda()
                rec_indices = None

                send_indices = torch.vstack((edge_node_mat_graph_idx, edge_node_mat_edge_idx, edge_node_mat_send_node_idx))
                edge_send_mat = torch.sparse.DoubleTensor(torch.LongTensor(send_indices), edge_mat_values, sparse_edge_node_mat_shape)
                edge_send_mat = edge_send_mat.cuda()
                send_indices = None


                opt.zero_grad()           
                predicted_acc = model.cmpt_net_force(feat_train, edge_rec_mat, edge_send_mat)

                loss = torch.sum(torch.abs(acc_train - predicted_acc) )

                loss.backward()
                opt.step()
                sched.step()
                total_loss += loss.item()

                num_items += len(acc_train) * args.num_nodes # the number of nodes in this batch

            training_loss = total_loss / num_items

            print("Epoch: {}, training loss: {}".format(epoch, training_loss))

            # save the model current epoch
            saving_path = saving_dir + "checkpoint_{}.pt".format(epoch)
            checkpoint = { 
                'epoch': epoch,
                'model': model.state_dict(),
                'opt': opt.state_dict(),
                'lr_sched': sched,
                'training_loss': training_loss,
            }
            torch.save(checkpoint, saving_path)


         