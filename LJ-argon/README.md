## Experiments with LJ-argon

Implementation of PIG'N'PI based on sparse tensors for the LJ system.  

Authors: Zhichao Han, David S. Kammer and Olga Fink.  

(MIT license)  


### Download data

The data can be downloaded via [this link](https://www.research-collection.ethz.ch/handle/20.500.11850/563126). The two parts data should be extracted to the same folder. Then, the path to this folder should be provided to train the model (e.g., line 83 in `pairwise_force/train_force_main.py`). 

### Learn pairwise force

1. `cd pairwise_force/`  
2. `python train_force_main.py`  
   (evalute the trained model)  
3. `python test_trained_force_model.py`  

Note that we train PIG'N'PI, GN+ and baseline for 500 epochs to learn the pairwise force.  


### Learn pairwise potential energy

1. `cd pairwise_potential/`  
2. `python train_M_times.py`  
   (evalute the trained model)  
3. `python test_trained_potential_model.py`  
