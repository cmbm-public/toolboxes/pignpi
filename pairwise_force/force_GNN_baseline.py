'''
The baseline model implementation is from: 
https://github.com/MilesCranmer/symbolic_deep_learning/blob/master/models.py (MIT license)
'''

import numpy as np
import torch
from torch import nn
from torch.functional import F
from torch.optim import Adam
from torch_geometric.nn import MetaLayer, MessagePassing
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, Softplus, Sigmoid
from torch.autograd import Variable, grad

torch.set_default_dtype(torch.float64)

# the original model in paper
class GN_original(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_original, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            ReLU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
            Lin(hidden, hidden),
            ReLU(),
#             Lin(hidden, hidden),
#             ReLU(),
            Lin(hidden, ndim)
        )
    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]

        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]




class SiLU(torch.nn.Module):    
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str



# the original model in paper
class GN_baseline_SiLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_baseline_SiLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.
        self.msg_fnc = Seq(
            Lin(2*n_f, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            ##(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            SiLU(),
            Lin(hidden, msg_dim)
        )
        
        self.node_fnc = Seq(
            Lin(msg_dim+n_f, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
            Lin(hidden, hidden),
            SiLU(),
#             Lin(hidden, hidden),
#             ReLU(),
            Lin(hidden, ndim)
        )
    
    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]
        x = x
        return self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x)
      
    def message(self, x_i, x_j):
        # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
        tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.msg_fnc(tmp)
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]

        tmp = torch.cat([x, aggr_out], dim=1)
        return self.node_fnc(tmp) #[n, nupdate]





