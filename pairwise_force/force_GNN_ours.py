'''
The modules of PIGNPI, with different activation functions
'''

import numpy as np
import torch
from torch import nn
from torch.functional import F
from torch.optim import Adam
from torch_geometric.nn import MetaLayer, MessagePassing
from torch.nn import Sequential as Seq, Linear as Lin, ReLU, GELU, Tanh, Softplus, Sigmoid, LeakyReLU
from torch.autograd import Variable, grad

torch.set_default_dtype(torch.float64)



class SiLU(torch.nn.Module):    
    # from https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#SiLU, since it is missing in 1.6.0
    def __init__(self, inplace = False):
        super(SiLU, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return x.mul_(x.sigmoid() ) if self.inplace else x.mul(x.sigmoid() )

    def extra_repr(self):
        inplace_str = 'inplace=True' if self.inplace else ''
        return inplace_str



# PIGNPI with SiLU (default)
class GN_force_SiLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_SiLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_SiLU, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            SiLU(),

            Lin(hidden, hidden),
            SiLU(),

            Lin(hidden, hidden),
            SiLU(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            SiLU(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



# PIGNPI with ReLU
class GN_force_ReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_ReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_ReLU, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            ReLU(),

            Lin(hidden, hidden),
            ReLU(),

            Lin(hidden, hidden),
            ReLU(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            ReLU(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



# PIGNPI with GELU
class GN_force_GELU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_GELU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_GELU, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            GELU(),

            Lin(hidden, hidden),
            GELU(),

            Lin(hidden, hidden),
            GELU(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            GELU(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here


# PIGNPI with Tanh
class GN_force_Tanh(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_Tanh, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_Tanh, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            Tanh(),

            Lin(hidden, hidden),
            Tanh(),

            Lin(hidden, hidden),
            Tanh(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Tanh(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



# PIGNPI with Sigmoid
class GN_force_Sigmoid(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_Sigmoid, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_Sigmoid, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            Sigmoid(),

            Lin(hidden, hidden),
            Sigmoid(),

            Lin(hidden, hidden),
            Sigmoid(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Sigmoid(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



# PIGNPI with Softplus
class GN_force_Softplus(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_Softplus, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_Softplus, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            Softplus(),

            Lin(hidden, hidden),
            Softplus(),

            Lin(hidden, hidden),
            Softplus(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            Softplus(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



# PIGNPI with LeakyReLU
class GN_force_LeakyReLU(MessagePassing):
    def __init__(self, n_node, n_f, msg_dim, ndim, hidden=300, aggr='add', flow="target_to_source"):
        super(GN_force_LeakyReLU, self).__init__(aggr=aggr, flow=flow)  # "Add" aggregation.

        torch.set_default_dtype(torch.float64)
        self.ndim = ndim
        self.n_node = n_node
        print("In GN_force_LeakyReLU, n_node is : {}".format(self.n_node) )

        self.msg_fnc_type0 = Seq(
            Lin(2*n_f, hidden), # pos, vel, charge, mass; for two nodes
            LeakyReLU(),

            Lin(hidden, hidden),
            LeakyReLU(),

            Lin(hidden, hidden),
            LeakyReLU(),

            #(Can turn on or off this layer:)
            Lin(hidden, hidden), 
            LeakyReLU(),
            
            Lin(hidden, msg_dim)
        )
        

    def forward(self, x, edge_index, edge_feature):
        #x is [n, n_f]

        if edge_feature == None:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = None)
        else:
            predicted_force = self.propagate(edge_index, size=(x.size(0), x.size(0)), x=x, edge_feature_list = edge_feature)
        
        predicted_acc = predicted_force / x[:, -1][:, None] # add edges from all types, then divide the mass
        return predicted_acc
      

    def message(self, x_i, x_j, edge_feature_list):
        if edge_feature_list == None:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        else:
            # x_i has shape [n_e, n_f]; x_j has shape [n_e, n_f]
            # edge_feature_list has shape [n_e, n_ef]
            tmp = torch.cat([x_i, x_j, edge_feature_list], dim=1)
        
        return self.msg_fnc_type0(tmp)
        
    
    def update(self, aggr_out, x=None):
        # aggr_out has shape [n, msg_dim]
        # tmp = torch.cat([x, aggr_out], dim=1)
        # return aggr_out / x[:, -1][:, None]
        
        return aggr_out # not dividing mass here



