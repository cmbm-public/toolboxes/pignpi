'''
Script to evaluate the model trained on noisy training dataset, with noisy test dataset
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
import torch
torch.set_num_threads(1)

from torch.autograd import Variable
from matplotlib import pyplot as plt
import pickle as pkl
from torch_geometric.data import Data
from torch_geometric.data import DataLoader
from tqdm import tqdm
import math
import random



from force_GNN_baseline import GN_original
from force_GNN_ours import GN_force_SiLU


np.random.rand(123)
torch.manual_seed(123)
random.seed(123)


# model_flag = "ours"
# model_flag = "original"


## the simulation type
# sim_type = "spring"
# sim_type = "charge"
# sim_type = "orbital"
# sim_type = "discontinuous"


# simulation dimension
# data_dim = 3 # 
# data_dim = 2

noisy_type = "2" # condition on the "frame" of the camera, used in our paper (A.8)


for model_flag in ["ours"]: # "ours" for PIGNPI, "original" for the baseline model
    for sim_type in ["spring", "charge", "orbital", "discontinuous"]:
        for data_dim in [2, 3]:            
            for noise_level in [1e-7, 5e-7, 1e-6, 5e-6, 1e-5]:
            # PIGNPI: SiLU, baseline: ReLU

                if model_flag == "ours":                    
                    if noisy_type == "1":
                        saving_dir = "./trained_model_{}/ours_dim{}_noise{}/".format(sim_type, data_dim, noise_level)
                    elif noisy_type == "2":
                        saving_dir = "./trained_model_{}/ours_dim{}_noise{}_absolute_white_noise/".format(sim_type, data_dim, noise_level)
                    print("--------\nTo save our model in {}".format(saving_dir) )
                elif model_flag == "original":                    
                    if noisy_type == "1":
                        saving_dir = "./trained_model_{}/baseline_dim{}_noise{}/".format(sim_type, data_dim, noise_level)
                    elif noisy_type == "2":
                        saving_dir = "./trained_model_{}/baseline_dim{}_noise{}_absolute_white_noise/".format(sim_type, data_dim, noise_level)
                    print("--------\nTo save baseline model in {}".format(saving_dir) )
                else:
                    print("model_flag error")
                    exit()
                assert os.path.isdir(saving_dir)


                torch.set_default_dtype(torch.float64)
                square_flag = False
                augment_flag = False



                # assert we have gpu
                print(torch.cuda.device_count())
                print(torch.ones(1).cuda())

                simulation_data_dir = "../simulation/noisy_{}_simulation/".format(sim_type)
                if noisy_type == "1":
                    noisy_data_file_name = "{}_dim{}_noiseLevel{}.pkl".format(sim_type, data_dim, noise_level)
                    with open(simulation_data_dir+ noisy_data_file_name, "rb") as f:
                        simulation_data = pkl.load(f)
                        print("loading: " + simulation_data_dir + noisy_data_file_name)
                elif noisy_type == "2":
                    noisy_data_file_name = "absolute_white_noise_{}_dim{}_noiseLevel{}.pkl".format(sim_type, data_dim, noise_level)
                    with open(simulation_data_dir+ noisy_data_file_name, "rb") as f:
                        simulation_data = pkl.load(f)
                        print("loading: " + simulation_data_dir + noisy_data_file_name)


            
                acc = simulation_data["node_acc"]
                num_experiments, simulation_steps, num_nodes, dim = acc.shape
                print("num_experiments, simulation_steps, num_nodes, dim")
                print(num_experiments, simulation_steps, num_nodes, dim)



                ## load the force and potential on edges
                true_force_potential_dir = "../simulation/{}_simulation/".format(sim_type)
                with open(true_force_potential_dir+ "force_potential_{}_dim{}.pkl".format(sim_type, data_dim), "rb") as f:
                    force_potential_groundTruth = pkl.load(f)
                    print("loaded " + true_force_potential_dir+ "force_potential_{}_dim{}.pkl".format(sim_type, data_dim) )
                    edge_force = force_potential_groundTruth['edge_force']
                    edge_potential = force_potential_groundTruth['edge_potential']

                print("edge_force shape")
                print(edge_force.shape)
                print("edge_potential shape")
                print(edge_potential.shape)

                 

                # edge_list and edge_feature
                edge_list = []
                # edge_feature = []
                for this_edge in simulation_data["edge_list"]:
                    edge_list.append([this_edge[0], this_edge[1] ] ) # edge from i to j
                    # edge_feature.append([this_edge[2] ] )
                    edge_list.append([this_edge[1], this_edge[0] ] ) # edge from j to i
                    # edge_feature.append([this_edge[2] ] )
                edge_list = np.array(edge_list) # shape [num_edge, 2]
                # edge_feature = np.array(edge_feature) # shape [num_edge, 1], 1 is the spring constant

                


                ## construct feature
                # mass
                node_mass = np.array(simulation_data["node_mass"])
                print("node_mass: {}".format(node_mass ) )
                node_mass = node_mass[:, np.newaxis]
                node_mass = np.tile(node_mass, (num_experiments, simulation_steps, 1, 1) )
                print("node_mass shape: {}".format( node_mass.shape) )


                # charge
                node_charge = np.array(simulation_data["node_charge"])
                print("node_charge: {}".format(node_charge) )
                node_charge = node_charge[:, np.newaxis]
                node_charge = np.tile(node_charge, (num_experiments, simulation_steps, 1, 1) )
                print("node_charge shape: {}".format( node_charge.shape) )

                node_feature = np.concatenate((simulation_data["node_location"], simulation_data["node_velocity"], node_charge, node_mass), axis=-1)
                print(node_feature.shape)

                node_potential = simulation_data["node_potential"]
                print("node_potential shape")
                print(node_potential.shape)


                ## split training dataset and testing dataste
                training_node_X = []
                training_node_Y = []
                training_edge_force = [] #for evaluting the pairwise force on edges
                training_edge_potential = [] #for evaluting the potential on edges
                valid_node_X = []
                valid_node_Y = []
                valid_edge_force = []
                valid_edge_potential = []
                testing_node_X = []
                testing_node_Y = []
                testing_edge_force = []
                testing_edge_potential = []

                # for flattening first two dimensions
                node_feature_flatten = []
                acc_flatten = []
                edge_force_flatten = []
                edge_potential_flatten = []
                for i in range(num_experiments):
                    for j in range(simulation_steps):
                        node_feature_flatten.append(node_feature[i, j] )
                        acc_flatten.append(acc[i,j])
                        edge_force_flatten.append(edge_force[i,j])
                        edge_potential_flatten.append(edge_potential[i, j] )


                training_idx_list = simulation_data["training_idx"]
                val_idx_list = simulation_data["val_idx"]
                test_idx_list = simulation_data["test_idx"]



                # training dataset
                training_idx_list = training_idx_list[0 : len(training_idx_list) // 5]
                for train_idx in training_idx_list:
                    training_node_X.append(node_feature_flatten[train_idx])
                    training_node_Y.append(acc_flatten[train_idx])
                    training_edge_force.append(edge_force_flatten[train_idx] )
                    training_edge_potential.append(edge_potential_flatten[train_idx])

                # valid dataset
                val_idx_list = val_idx_list[0 : len(val_idx_list) // 5]
                for val_idx in val_idx_list:
                    valid_node_X.append(node_feature_flatten[val_idx])
                    valid_node_Y.append(acc_flatten[val_idx])
                    valid_edge_force.append(edge_force_flatten[val_idx])      
                    valid_edge_potential.append(edge_potential_flatten[val_idx])

                # testing dataset
                test_idx_list = test_idx_list[0 : len(test_idx_list) // 5]
                for test_idx in test_idx_list:
                    testing_node_X.append(node_feature_flatten[test_idx])
                    testing_node_Y.append(acc_flatten[test_idx])
                    testing_edge_force.append(edge_force_flatten[test_idx])
                    testing_edge_potential.append(edge_potential_flatten[test_idx])
                        


                training_node_X = np.array(training_node_X)
                training_node_Y = np.array(training_node_Y)
                training_edge_force = np.array(training_edge_force)
                training_edge_potential = np.array(training_edge_potential)

                valid_node_X = np.array(valid_node_X)
                valid_node_Y = np.array(valid_node_Y)
                valid_edge_force = np.array(valid_edge_force)
                valid_edge_potential = np.array(valid_edge_potential)

                testing_node_X = np.array(testing_node_X)
                testing_node_Y = np.array(testing_node_Y)
                testing_edge_force = np.array(testing_edge_force)
                testing_edge_potential = np.array(testing_edge_potential)


                print("training_node_X shape: {}".format(training_node_X.shape) )
                print("training_node_Y shape: {}".format(training_node_Y.shape) )
                print("training_edge_force shape: {}".format(training_edge_force.shape) )
                print("training_edge_potential shape: {}".format(training_edge_potential.shape) )

                print("valid_node_X shape: {}".format(valid_node_X.shape) )
                print("valid_node_Y shape: {}".format(valid_node_Y.shape) )
                print("valid_edge_force shape: {}".format(valid_edge_force.shape) )
                print("valid_edge_potential shape: {}".format(valid_edge_potential.shape) )

                print("testing_node_X shape: {}".format(testing_node_X.shape) )
                print("testing_node_Y shape: {}".format(testing_node_Y.shape) )
                print("testing_edge_force shape: {}".format(testing_edge_force.shape) )
                print("testing_edge_potential shape: {}".format(testing_edge_potential.shape) )




                # model configuration
                aggr = 'add'
                hidden = 300
                msg_dim = dim

                # n_f = data.shape[3]
                n_f = 2 * dim + 2 # node position, node velocity, node charge, node mass

                # configurate model
                if model_flag == "ours":                                                            
                    model = GN_force_SiLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                elif model_flag == "original":
                    model = GN_original(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()                    
                else:
                    print("not valide model")
                    exit()



                edge_list = np.transpose(edge_list)
                print("edge_list shape: {}".format(edge_list.shape) )
                print(edge_list.dtype)

                data_example = Data(
                    x = torch.from_numpy(training_node_X[0]),
                    edge_index = torch.tensor(edge_list, dtype=torch.long),
                    y=torch.from_numpy(training_node_Y[0]) )




                # batch_size = 1
                batch_size = 2
                # batch_size = 32

                training_example_number = len(training_node_X)
                print(edge_list.dtype)
                trainloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(training_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(training_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        edge_force_label = torch.from_numpy(training_edge_force[i]),
                        edge_potential_label = torch.from_numpy(training_edge_potential[i]) )  for i in range(training_example_number ) ],
                    batch_size=batch_size,
                    shuffle=True
                )



                valid_example_number = len(valid_node_X)
                validloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(valid_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(valid_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        edge_force_label = torch.from_numpy(valid_edge_force[i]),
                        edge_potential_label = torch.from_numpy(valid_edge_potential[i]) )  for i in range(valid_example_number) ],
                    batch_size = batch_size,
                    shuffle = False
                    )

                testing_example_number = len(testing_node_X)
                testloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(testing_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(testing_node_Y[i]),
                        # edge_feature = torch.from_numpy(edge_feature),
                        edge_force_label = torch.from_numpy(testing_edge_force[i]),
                        edge_potential_label = torch.from_numpy(testing_edge_potential[i]) )  for i in range(testing_example_number ) ],
                    batch_size=batch_size,
                    shuffle=False
                )


                total_epochs = 200 # training epochs

                model_path = saving_dir + "saved_checkpoint_before_training"
                checkpoint = torch.load(model_path )
                # print(checkpoint.keys()): dict_keys(['model', 'opt', 'lr_sched', 'training_loss', 'valid_loss', 'testing_loss'])

                min_valid_loss = checkpoint["valid_loss"]
                test_loss = 0.0
                train_loss = 0.0
                epoch_id = 0


                for epoch in tqdm(range(0, total_epochs)):
                    model_path = saving_dir + "saved_checkpoint_at_epoch_{}".format(epoch)
                    checkpoint = torch.load(model_path )
                    # model.load_state_dict(checkpoint['model'])

                    this_valid_loss = checkpoint['valid_loss']
                    this_test_loss = checkpoint['testing_loss']
                    if this_valid_loss < min_valid_loss:
                        min_valid_loss = this_valid_loss
                        test_loss = this_test_loss
                        training_loss = checkpoint['training_loss']
                        epoch_id = epoch

                print("\n\n" + "********" * 4)
                print("simulation: {}, NN model: {}, dim: {}, noise_level: {}".format(sim_type, model_flag, dim, noise_level ) )
                print("###Extract trained model")
                print("train_loss: {}, min_valid_loss: {}, test_loss: {}, epoch_id: {}".format(training_loss, min_valid_loss, test_loss, epoch_id) )

                # exit()
                # --------------------------

                # compute the Acceleration Error(predicted_acceleration, true_acceleration)
                def cmpt_loss(input_dataloader, ogn_model):
                    total_loss = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        # edge_feature = ginput.edge_feature.cuda()
                        edge_feature = None
                        # ginput.batch = ginput.batch.cuda() # https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html#mini-batches

                        predicted_y = ogn_model(x, edge_index, edge_feature)
                        if square_flag:
                            loss = torch.sum((true_y - predicted_y) **2 )
                        else:
                            loss = torch.sum(torch.abs(true_y - predicted_y) )
                        total_loss += loss.item()
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch
                    return total_loss / num_items





                #  run the best model, for testing loss and valid loss
                best_model_path = saving_dir + "saved_checkpoint_at_epoch_{}".format(epoch_id)
                best_checkpoint = torch.load(best_model_path )
                model.load_state_dict(best_checkpoint['model'])


                model.eval()
                with torch.no_grad():
                    print("###Acceleration loss")
                    # training_loss = cmpt_loss(trainloader, model)
                    # print(" Training Loss: {}".format(training_loss)) # traning loss is different, because optimizing the batches
                    valid_loss = cmpt_loss(validloader, model)
                    testing_loss = cmpt_loss(testloader, model)
                    print("{}, Valid Acceleration Loss: {}, Testing Acceleration Loss: {}".format(epoch_id, valid_loss, testing_loss) )


                # --------------------------

                # compute the Error on net force (predicted_force, true_force)
                def cmpt_force_error(input_dataloader, ogn_model):
                    total_error = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        # edge_feature = ginput.edge_feature.cuda()
                        edge_feature = None
                        
                        # true force
                        true_force = true_y * x[:, -1][:, None]

                        # predicted force by model
                        tmp = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)
                        if model_flag == "ours":
                            m12 = ogn_model.msg_fnc_type0(tmp)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp)   
                        else:
                            print("IN cmpt_force_error, model_flag error")
                            exit()         
                        aggragated_message = torch.zeros(x.size(0), msg_dim).cuda()
                        for j in range(edge_index.shape[1]):
                            send_node = edge_index[0][j]
                            aggragated_message[send_node] += m12[j]
                        predicted_force = aggragated_message
                        
                        # print(aggragated_message)
                        # exit()
                        # predicted_acc = ogn_model.node_fnc(torch.cat([x, aggragated_message], dim=1))


                        error = torch.sum(torch.abs(true_force - predicted_force) ) # the force error
                        # error = torch.sum((true_force - predicted_force)**2) # the force error

                        # error = torch.sum(torch.abs(predicted_force / x[:, -1][:, None] - true_y) ) # the Acceleration error
                        # error = torch.sum(torch.abs(predicted_acc - true_y) ) # the Acceleration error

                        
                        total_error += error.item()
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch


                    return total_error / num_items



                model.eval()
                with torch.no_grad():
                    print("###Force Error")
                    valid_force_error = cmpt_force_error(validloader, model )
                    test_force_error = cmpt_force_error(testloader, model )
                    print("{}, Valid Force Error: {}, Testing Force Error: {}".format(epoch_id, valid_force_error, test_force_error) )



                # --------------------------
                ## compute the Newton's Third Law error: MSE(F_ij+F_ji, 0)

                # compute the Symmetry Error(F_ij, -Fji)
                def cmpt_force_symmetry(input_dataloader, ogn_model):
                    total_error = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        # edge_feature = ginput.edge_feature.cuda()
                        edge_feature = None

                        tmp12 = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)
                        tmp21 = torch.cat([x[edge_index[1] ], x[edge_index[0] ] ], dim=1)        
                        if model_flag == "ours":
                            m12 = ogn_model.msg_fnc_type0(tmp12)
                            m21 = ogn_model.msg_fnc_type0(tmp21)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp12)
                            m21 = ogn_model.msg_fnc(tmp21)
                        else:
                            print("IN cmpt_force_symmetry, model_flag error")
                            exit()
                        # print("m12 shape")
                        # print(m12.shape) # (56*batch_size, 2)

                        # print("m21 shape")
                        # print(m21.shape)  # (56*batch_size, 2)

                        ## m12 should approximate -m21 
                        error = torch.sum(torch.abs(m12 + m21 ) )
                        total_error += error.item()
                        num_items += int(m12.shape[0])        
                        
                    return total_error / num_items
                        



                model.eval()
                with torch.no_grad():
                    print("###Symmetry Error (Newton Third Law)")
                    valid_force_symmetry_error = cmpt_force_symmetry(validloader, model )
                    test_force_symmetry_error = cmpt_force_symmetry(testloader, model )
                    print("{}, Valid Force Symmetry Error: {}, Testing Force Symmetry Error: {}".format(epoch_id, valid_force_symmetry_error, test_force_symmetry_error) )




                # --------------------------

                # compute the Error on edge force (predicted_edge_force, true_edge_force)
                def cmpt_edge_force_error(input_dataloader, ogn_model):
                    # 4. Force error on edges
                    total_error = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_force_label = ginput.edge_force_label.cuda()

                        edge_feature = None

                        # print(len(edge_index))
                        # print(edge_force_label.shape)

                        tmp12 = torch.cat([x[edge_index[0] ], x[edge_index[1] ] ], dim=1)

                        if model_flag == "ours":
                            m12 = ogn_model.msg_fnc_type0(tmp12)
                        elif model_flag == "original":
                            m12 = ogn_model.msg_fnc(tmp12)
                        else:
                            print("IN cmpt_edge_force_error, model_flag error")
                            exit()
                        # print(m12.shape)

                        predicted_edge_force = m12
                        error = torch.sum(torch.abs(predicted_edge_force -  edge_force_label ) )
                        total_error += error.item()
                        num_items += int(predicted_edge_force.shape[0])
                    return total_error / num_items
                        


                model.eval()
                with torch.no_grad():
                    print("###Force Error on Edges")
                    valid_edge_force_error = cmpt_edge_force_error(validloader, model )
                    test_edge_force_error = cmpt_edge_force_error(testloader, model )

                    print("{}, Valid Edge Force Error: {}, Testing Edge Force Error: {}".format(epoch_id, valid_edge_force_error, test_edge_force_error) )







                # 1. Acceleration loss
                # 2. Net force error on nodes
                # 3. Force symmetry error
                # 4. Force error on edges




