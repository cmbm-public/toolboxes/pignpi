'''
The script to train PIGNPI
'''
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

import numpy as np
import torch
from torch.autograd import Variable
from matplotlib import pyplot as plt
import pickle as pkl
from torch.optim import Adam
from torch_geometric.data import Data, DataLoader
from tqdm import tqdm
import math

from force_GNN_ours import GN_force_SiLU # PIGNPI use SiLU as default
# from force_GNN_ours import GN_force_SiLU, GN_force_ReLU, GN_force_GELU, GN_force_Tanh, GN_force_Sigmoid, GN_force_Softplus, GN_force_LeakyReLU

from force_GNN_baseline import GN_original # the baseline model
from force_GNN_baseline import GN_baseline_SiLU # the baseline model, change ReLU to SiLU


model_flag = "ours"
# model_flag = "original"
torch.set_default_dtype(torch.float64)
square_flag = False # use l1 loss



for model_flag in ["ours"]: # "ours" for PIGNPI; or "original" for the baseline model.
    for sim_type in ["spring"]: # we test ["spring", "charge", "orbital", "discontinuous"] in the paper
    # for sim_type in ["spring", "charge", "orbital", "discontinuous"]:
        for data_dim in [2]: # dimension, 2 or 3
        # for data_dim in [2, 3]:
            for act_func in ["SiLU"]: # You can test other activation functions by changing here. PIGNPI use SiLU as default. The baseline model use ReLU as default
            # for act_func in ["SiLU", "ReLU", "GELU", "tanh", "sigmoid", "softplus", "LeakyReLU"]:
                if model_flag == "ours":
                    saving_dir = "./trained_model_{}/ours_{}_{}/".format(sim_type, act_func, data_dim)
                    print("--------\nTrain PIGNPI with {}".format(act_func) )
                elif model_flag == "original":                    
                    saving_dir = "./trained_model_{}/original_{}_{}/".format(sim_type, act_func, data_dim)
                    print("--------\nTrain original model")
                else:
                    print("model_flag error")
                    exit()
                # assert os.path.isdir(saving_dir)
                if not os.path.isdir(saving_dir):                
                    from pathlib import Path
                    Path(saving_dir).mkdir(parents=True, exist_ok=False)


                # TODO: assert saving_dir exsits

                # assert we have gpu
                print(torch.cuda.device_count())
                print(torch.ones(1).cuda())


                ## load simulation data
                simulation_data_dir = "../simulation/{}_simulation/".format(sim_type)
                with open(simulation_data_dir+ "{}_dim{}.pkl".format(sim_type, data_dim), "rb") as f:
                    simulation_data = pkl.load(f)
                acc = simulation_data["node_acc"]
                num_experiments, simulation_steps, num_nodes, dim = acc.shape
                print("num_experiments, simulation_steps, num_nodes, dim")
                print(num_experiments, simulation_steps, num_nodes, dim)

                # edge_list and edge_feature
                edge_list = []
                edge_feature = []
                for this_edge in simulation_data["edge_list"]:
                    edge_list.append([this_edge[0], this_edge[1] ] ) # edge from i to j
                    edge_list.append([this_edge[1], this_edge[0] ] ) # edge from j to i
                edge_list = np.array(edge_list) # shape [num_edge, 2]


                print(edge_list.shape)
                # print(edge_list)

                # exit()

                ## construct feature
                # mass
                node_mass = np.array(simulation_data["node_mass"])
                print("node_mass: {}".format(node_mass ) )
                node_mass = node_mass[:, np.newaxis]
                node_mass = np.tile(node_mass, (num_experiments, simulation_steps, 1, 1) )
                print("node_mass shape: {}".format( node_mass.shape) )


                # charge
                node_charge = np.array(simulation_data["node_charge"])
                print("node_charge: {}".format(node_charge) )
                node_charge = node_charge[:, np.newaxis]
                node_charge = np.tile(node_charge, (num_experiments, simulation_steps, 1, 1) )
                print("node_charge shape: {}".format( node_charge.shape) )

                node_feature = np.concatenate((simulation_data["node_location"], simulation_data["node_velocity"], node_charge, node_mass), axis=-1)
                print(node_feature.shape)

                node_potential = simulation_data["node_potential"]
                print("node_potential shape")
                print(node_potential.shape)


                ## split training dataset and testing dataste
                training_node_X = []
                training_node_Y = []
                valid_node_X = []
                valid_node_Y = []
                testing_node_X = []
                testing_node_Y = []

                # for flattening first two dimensions
                node_feature_flatten = []
                acc_flatten = []
                for i in range(num_experiments):
                    for j in range(simulation_steps):
                        node_feature_flatten.append(node_feature[i, j] )
                        acc_flatten.append(acc[i,j])


                training_idx_list = simulation_data["training_idx"]
                val_idx_list = simulation_data["val_idx"]
                test_idx_list = simulation_data["test_idx"]




                # training dataset
                training_idx_list = training_idx_list[0 : len(training_idx_list) // 5]
                for train_idx in training_idx_list:
                    training_node_X.append(node_feature_flatten[train_idx])
                    training_node_Y.append(acc_flatten[train_idx])

                # valid dataset
                val_idx_list = val_idx_list[0 : len(val_idx_list) // 5]
                for val_idx in val_idx_list:
                    valid_node_X.append(node_feature_flatten[val_idx])
                    valid_node_Y.append(acc_flatten[val_idx])        

                # testing dataset
                test_idx_list = test_idx_list[0 : len(test_idx_list) // 5]
                for test_idx in test_idx_list:
                    testing_node_X.append(node_feature_flatten[test_idx])
                    testing_node_Y.append(acc_flatten[test_idx])
                        


                training_node_X = np.array(training_node_X)
                training_node_Y = np.array(training_node_Y)
                valid_node_X = np.array(valid_node_X)
                valid_node_Y = np.array(valid_node_Y)
                testing_node_X = np.array(testing_node_X)
                testing_node_Y = np.array(testing_node_Y)


                print("training_node_X shape: {}".format(training_node_X.shape) )
                print("training_node_Y shape: {}".format(training_node_Y.shape) )
                print("valid_node_X shape: {}".format(valid_node_X.shape) )
                print("valid_node_Y shape: {}".format(valid_node_Y.shape) )
                print("testing_node_X shape: {}".format(testing_node_X.shape) )
                print("testing_node_Y shape: {}".format(testing_node_Y.shape) )




                # model configuration
                aggr = 'add'
                hidden = 300
                msg_dim = dim

                # n_f = data.shape[3]
                n_f = 2 * dim + 2 # node position, node velocity, node charge, node mass


                if model_flag == "ours":
                    # ["SiLU", "ReLU", "GELU", "tanh", "sigmoid", "softplus", "LeakyReLU"]:
                    # GN_force_SiLU, GN_force_ReLU, GN_force_GELU, GN_force_Tanh, GN_force_Sigmoid, GN_force_Softplus, GN_force_LeakyReLU
                    if act_func == "SiLU": #default
                        model = GN_force_SiLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "ReLU":
                        model = GN_force_ReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "GELU":
                        model = GN_force_GELU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "tanh":
                        model = GN_force_Tanh(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "sigmoid":
                        model = GN_force_Sigmoid(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "softplus":
                        model = GN_force_Softplus(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "LeakyReLU":
                        model = GN_force_LeakyReLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim, hidden=hidden, aggr=aggr).cuda()
                    else:
                        print("act_func flag error")
                        exit()
                elif model_flag == "original":
                    if act_func == "SiLU":
                        model = GN_baseline_SiLU(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                    elif act_func == "ReLU": # default
                        model = GN_original(n_node = num_nodes, n_f=n_f, msg_dim=msg_dim, ndim=dim,  hidden=hidden, aggr=aggr).cuda()
                else:
                    print("not valide model")
                    exit()



                edge_list = np.transpose(edge_list)
                print("edge_list shape: {}".format(edge_list.shape) )
                print(edge_list.dtype)

                data_example = Data(
                    x = torch.from_numpy(training_node_X[0]),
                    edge_index = torch.tensor(edge_list, dtype=torch.long),
                    y=torch.from_numpy(training_node_Y[0]) )

                # print(data_example)
                # print(data_example.x)
                # print(data_example.y)
                # print(data_example.edge_index)


                # batch_size = 1
                # batch_size = 2
                batch_size = 32
                # batch_size  = 32
                training_example_number = len(training_node_X)
                print(edge_list.dtype)
                trainloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(training_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(training_node_Y[i]) )  for i in range(training_example_number ) ],
                    batch_size=batch_size,
                    shuffle=True
                )

                valid_example_number = len(valid_node_X)
                validloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(valid_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(valid_node_Y[i]) )  for i in range(valid_example_number) ],
                    batch_size = batch_size,
                    shuffle = False
                    )

                testing_example_number = len(testing_node_X)
                testloader = DataLoader(
                    [Data(
                        x = torch.from_numpy(testing_node_X[i]),
                        edge_index = torch.tensor(edge_list, dtype=torch.long),
                        y = torch.from_numpy(testing_node_Y[i]) )  for i in range(testing_example_number ) ],
                    batch_size=batch_size,
                    shuffle=False
                )

                # for data_example in trainloader:
                #     print(data_example)
                #     print(data_example.x)
                #     print(data_example.y)
                #     print(data_example.edge_index)
                #     print(model(data_example.x.cuda(), data_example.edge_index.cuda(), data_example.edge_feature.cuda() ) )
                #     break

                # exit()



                from torch.optim.lr_scheduler import ReduceLROnPlateau, OneCycleLR


                def cmpt_loss(input_dataloader, ogn_model):
                    total_loss = 0.0; num_items = 0
                    for ginput in input_dataloader:
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None
                        # ginput.batch = ginput.batch.cuda() # https://pytorch-geometric.readthedocs.io/en/latest/notes/introduction.html#mini-batches
                        predicted_y = ogn_model(x, edge_index, edge_feature)
                        if square_flag:
                            loss = torch.sum((true_y - predicted_y) **2 )
                        else:
                            loss = torch.sum(torch.abs(true_y - predicted_y) )
                        total_loss += loss.item()
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch
                    return total_loss / num_items



                init_lr = 1e-3
                # init_lr = 5e-4

                opt = torch.optim.Adam(model.parameters(), lr=init_lr, weight_decay=1e-8)
                total_epochs = 200 # training epochs

                batch_per_epoch = math.ceil(training_example_number / batch_size )

                sched = OneCycleLR(opt, max_lr=init_lr,
                                   steps_per_epoch=batch_per_epoch,#len(trainloader),
                                   epochs=total_epochs, final_div_factor=1e5)

                print("batch_per_epoch: {}".format(batch_per_epoch))

                # print("model.parameters()")
                # for name, param in model.named_parameters():
                #     print(name, param)

                assert next(model.parameters()).is_cuda

                ############# before training
                initial_training_loss = cmpt_loss(trainloader, model)
                print("Initial Training Loss: {}".format(initial_training_loss))

                initial_valid_loss = cmpt_loss(validloader, model)
                print("Initial Valid Loss: {}".format(initial_valid_loss))

                initial_testing_loss = cmpt_loss(testloader, model)
                print("Initial Testing Loss: {}".format(initial_testing_loss))

                saving_path = saving_dir + "saved_checkpoint_before_training"
                checkpoint = { 
                    'model': model.state_dict(),
                    'opt': opt.state_dict(),
                    'lr_sched': sched,
                    'training_loss': initial_training_loss,
                    'valid_loss' : initial_valid_loss,
                    'testing_loss': initial_testing_loss,
                }
                print("saving {}".format(saving_path))
                torch.save(checkpoint, saving_path)


                ############# start training
                for epoch in tqdm(range(0, total_epochs)):
                # for epoch in range(3):

                    total_loss = 0.0
                    i = 0
                    num_items = 0
                    # the original code here can deal with the cases: 1. batch_per_epoch > len(trainloader); 2. batch_per_epoch < len(trainloader)
                    # but let's go through the whole training dataset per epoch
                    # while i < batch_per_epoch:
                    for ginput in trainloader:
                        # if i >= batch_per_epoch:
                        #     break
                        opt.zero_grad()
                        x = ginput.x.cuda()
                        true_y = ginput.y.cuda()
                        edge_index = ginput.edge_index.cuda()
                        edge_feature = None

                        predicted_y = model(x, edge_index, edge_feature)

                        if square_flag:
                            loss = torch.sum((true_y - predicted_y) **2 )
                        else:
                            loss = torch.sum(torch.abs(true_y - predicted_y) )

                        loss.backward()
                        opt.step()
                        sched.step()
                        total_loss += loss.item()
                        i += 1
                        num_items += int(ginput.batch.shape[-1]) # all the nodes number in this batch

                    training_loss = total_loss / num_items # the average on the node level
                    valid_loss = cmpt_loss(validloader, model)
                    testing_loss = cmpt_loss(testloader, model)
                    if epoch % 5 == 0:
                        print("LOSS -- train: {}; val: {}; test:{}".format(training_loss, valid_loss, testing_loss) )


                    if epoch % 1 == 0:
                        saving_path = saving_dir + "saved_checkpoint_at_epoch_{}".format(epoch)
                        checkpoint = { 
                            'epoch': epoch,
                            'model': model.state_dict(),
                            'opt': opt.state_dict(),
                            'lr_sched': sched,
                            'training_loss': training_loss,
                            'valid_loss': valid_loss,
                            'testing_loss': testing_loss,
                            # 'cur_msgs_test': cur_msgs_test,
                            # 'cur_msgs_train': cur_msgs_train
                        }
                        # print("saving {}".format(saving_path))
                        torch.save(checkpoint, saving_path)

                    
                    
