# Physics-induced Graph Network for Particle Interaction (PIG'N'PI)

PIG'N'PI learns the pairwise force or pairwise potential energy from the trajectories of interacting particles.  

Authors: Zhichao Han, David S. Kammer and Olga Fink.  

(MIT license)  



### Requirements
Python dependencies:  
-- Python>=3.6.12  
-- Pytorch>=1.6.0  
-- [pytorch-geometric](https://pytorch-geometric-temporal.readthedocs.io/en/latest/notes/installation.html)>=1.6.3  
-- Numpy>=1.19.2  

Installation of pytorch-geometric can take about half hour depending on the local environment.  

Besides, we assume GPU is available.  We conduct the experiments on a Linux server with RTX 2080Ti, cudatoolkit=10.1.243.  

We thank the authors of the baseline model that we considered in our paper for releasing their code. Our implementation is developed based on their [repository](https://github.com/MilesCranmer/symbolic_deep_learning).  


### (Demo) Example: Train PIG'N'PI to learn pairwise force on 2D spring dataset
1. `cd pairwise_force/`  
2. `python train.py`  
The trained model will be saved into trained_model_spring/ours_SiLU_2/ (it means our PIGNPI model with SiLU activation function on the 2D spring data). Training will take about 12 minutes on a RTX 2080Ti.  
3. `python evalution.py`, which will use the validation dataset to choose the best trained epoch and run the test on testing dataset  



### Instruction to re-produce the results in our paper
For training the model to learn pairwise force on different datasets, with different activation functions:  
1. `cd pairwise_force/`  
2. modify line 32-39 in train.py, choose alternative dataset and activation function (follow the comments in codes)  
3. `python train.py`  
4. modify line 38-44 in evalution.py or line 40-45 in evaluate_generalization.py (follow the comments in codes)  
5. `python evalution.py` or `python evaluate_generalization.py`  


For training the model to learn pairwise force with noisy input:  
1. `cd pairwise_force/`  
2. modify line 36-42 in train_noisy_input.py (see comments in the code for details)  
3. `python train_noisy_input.py`  
4. modify line 50-53 in evaluate_noise.py (see comments in the code for details)  
5. `python evaluate_noise.py`  



For training the model to learn potential energy on different datasets, with different activation functions:  
1. `cd pairwise_potential_energy/`  
2. modify line 23-30 in main_potential.py (see comments in the code for details)  
3. `python main_potential.py`  
4. modify line 39-45 in evaluate_potential.py or line 39-45 in evaluate_generalization_potential.py (see comments in the code for details)  
5. `python evaluate_potential.py` or `python evaluate_generalization_potential.py`  